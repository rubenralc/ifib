package com.rubenralc.ifib.app.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Ruben on 16/01/14.
 */
public class VisitStateSQLiteHelper extends SQLiteOpenHelper {

    String sqlCreate = "CREATE TABLE VisitState(museum VARCHAR(200), area VARCHAR(200), room VARCHAR(200), state INTEGER)";

    private static VisitStateSQLiteHelper instance;

    public static VisitStateSQLiteHelper getInstance(Context context){
        if (instance != null)
            return instance;

        return new VisitStateSQLiteHelper(context, "imuseumsDB", null, 5);
    }

    private VisitStateSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory cursorFactory, int version) {
        super(context, name, cursorFactory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlCreate);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS VisitState");
        db.execSQL(sqlCreate);
    }
}
