package com.rubenralc.ifib.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.rubenralc.ifib.app.R;
import com.rubenralc.ifib.app.model.Route;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Ruben on 17/10/13.
 */
public class RouteAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Route> routes;

    public RouteAdapter(Context context, ArrayList<Route> routes) {
        this.context = context;
        this.routes = routes;
    }

    @Override
    public int getCount() {
        return routes.size();
    }

    @Override
    public Object getItem(int i) {
        return routes.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        RouteViewHolder routeViewHolder;

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.room_item, null);

            routeViewHolder = new RouteViewHolder();

            routeViewHolder.routeName = (TextView) view.findViewById(R.id.room_name);

            view.setTag(routeViewHolder);
        } else {
            routeViewHolder = (RouteViewHolder) view.getTag();
        }
        Route route = routes.get(i);
        if (route != null) {
            routeViewHolder.routeName.setText(route.getName(Locale.getDefault().getLanguage()));
        }

        return view;
    }

    private static class RouteViewHolder {
        TextView routeName;
    }
}
