package com.rubenralc.ifib.app.model;

import android.os.Environment;

import java.util.HashMap;

/**
 * Created by Ruben on 8/06/13.
 */
public class Museum {
    private String name;
    private String largeName;
    private String img;
    private HashMap<String, String> description;
    private boolean shoppingCart;
    private HashMap<String, String> sponsorsLiteral;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setLargeName(String largeName) {
        this.largeName = largeName;
    }

    public String getLargeName() {
        return largeName;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getDescription(String lang) {
        return description.get(lang);
    }

    public void setDescription(String lang, String description) {
        if (this.description == null)
            this.description = new HashMap<String, String>();

        this.description.put(lang, description);
    }

    public boolean isShoppingCart() {
        return shoppingCart;
    }

    public void setShoppingCart(boolean shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    public String getSponsorsLiteral(String lang) {
        return sponsorsLiteral.get(lang);
    }

    public void setSponsorsLiteral(String lang, String sponsorsLiteral) {
        if (this.sponsorsLiteral == null)
            this.sponsorsLiteral = new HashMap<String, String>();

        this.sponsorsLiteral.put(lang, sponsorsLiteral);
    }
}
