package com.rubenralc.ifib.app.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Ruben on 2/01/14.
 */
public class FavoritesSQLiteHelper extends SQLiteOpenHelper {

    String sqlCreate = "CREATE TABLE Favorites(id VARCHAR(200), name VARCHAR(200), img VARCHAR(200), museum VARCHAR(200), points INTEGER)";

    private static FavoritesSQLiteHelper instance;

    public static FavoritesSQLiteHelper getInstance(Context context){
        if (instance != null)
            return instance;

        return new FavoritesSQLiteHelper(context, "imuseumsDB", null, 5);
    }

    private FavoritesSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory cursorFactory, int version) {
        super(context, name, cursorFactory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlCreate);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS Favorites");
        db.execSQL(sqlCreate);
    }
}
