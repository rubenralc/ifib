package com.rubenralc.ifib.app.model;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Parcel;
import android.os.Parcelable;

import com.rubenralc.ifib.app.R;
import com.rubenralc.ifib.app.database.IMuseumsSQLiteHelper;
import com.rubenralc.ifib.app.model.interfaces.IDrawable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by Ruben on 19/06/13.
 */
public class Room implements Parcelable, IDrawable {
    private String id;
    private HashMap<String, String> name;
    private String type;
    private HashMap<String, String> descriptionPath;
    private ArrayList<Space> spaces;
    private int x;
    private int y;
    private int width;
    private int height;
    private float ratioWidth;
    private float ratioHeight;
    private boolean actual;
    private boolean visited;

    private int bgColor = Color.argb(255, 255, 255, 125);
    private String area;
    private String museum;

    public Room() {

    }

    public Room(Parcel in) {
        spaces = new ArrayList<Space>();
        readFromParcel(in);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName(String lang) {
        return name.get(lang);
    }

    public void setName(String lang, String name) {
        if (this.name == null)
            this.name = new HashMap<String, String>();
        this.name.put(lang, name);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescriptionPath(String lang) {
        return descriptionPath.get(lang);
    }

    public void setDescriptionPath(String lang, String descriptionPath) {
        if (this.descriptionPath == null)
            this.descriptionPath = new HashMap<String, String>();
        this.descriptionPath.put(lang, descriptionPath);
    }

    public ArrayList<Space> getSpaces() {
        return spaces;
    }

    public void setSpaces(ArrayList<Space> spaces) {
        this.spaces = spaces;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public float getOnScreenX() {
        return x * ratioWidth + 16;
    }

    public float getOnScreenY() {
        return y * ratioHeight + 16;
    }

    public float getOnScreenWidth() {
        return width * ratioWidth;
    }

    public float getOnScreenHeight() {
        return height * ratioHeight;
    }

    public void setRatioWidth(float ratioWidth) {
        this.ratioWidth = ratioWidth;
    }

    public void setRatioHeight(float ratioHeight) {
        this.ratioHeight = ratioHeight;
    }

    public void setActual(boolean actual) {
        this.actual = actual;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public Canvas draw(Context context, Canvas canvas) {

        RectF shape = new RectF(getOnScreenX(), getOnScreenY(), getOnScreenX() + getOnScreenWidth(), getOnScreenY() + getOnScreenHeight());

        Paint filling = new Paint();
        if (type.equals("artwork"))
            filling.setColor(bgColor);

        if (type.equals("gift_shop"))
            filling.setColor(Color.CYAN);

        if (type.equals("bathroom"))
            filling.setColor(Color.WHITE);

        if (type.equals("garden"))
            filling.setColor(Color.argb(255, 125, 255, 125));

        if (type.equals("cafe"))
            filling.setColor(Color.MAGENTA);

        if (type.equals("stairs"))
            filling.setColor(Color.GRAY);

        if (type.equals("reception"))
            filling.setColor(Color.DKGRAY);

        if (type.equals("video") || type.equals("classroom"))
            filling.setColor(Color.BLUE);

        if (type.equals("private"))
            filling.setColor(Color.RED);

        if (type.equals("corridor"))
            filling.setColor(Color.GRAY);

        if (type.equals("neutral"))
            filling.setColor(Color.LTGRAY);

        if (type.equals("computer"))
            filling.setColor(Color.argb(255, 173, 216, 230));

        filling.setStyle(Paint.Style.FILL);

        canvas.drawRect(shape, filling);

        Paint border = new Paint();
        border.setColor(Color.BLACK);
        border.setStyle(Paint.Style.STROKE);

        canvas.drawRect(shape, border);

        if (type.equals("artwork")) {
            String roomName = name.get(Locale.getDefault().getLanguage());
            Paint text = new Paint();
            text.setColor(Color.BLACK);
            text.setTextSize(text.getTextSize() + 6);
            Rect textBounds = new Rect();
            text.getTextBounds(roomName, 0, roomName.length(), textBounds);

            float textX = (getOnScreenX() + (getOnScreenWidth() / 2)) - (textBounds.width() / 2);
            float textY = (getOnScreenY() + (getOnScreenHeight() / 2)) - (textBounds.height() / 2);

            canvas.drawText(roomName, textX, textY, text);
        } else {
            Paint icon = new Paint();
            Bitmap bIcon = null;

            if (type.equals("gift_shop"))
                bIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.gift_shop);

            if (type.equals("bathroom"))
                bIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.bathroom);

            if (type.equals("garden"))
                bIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.garden);

            if (type.equals("cafe"))
                bIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.cafe);

            if (type.equals("stairs"))
                bIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.stairs);

            if (type.equals("reception"))
                bIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.reception);

            if (type.equals("video") || type.equals("classroom") || type.equals("computer"))
                bIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.video);

            if (!type.equals("corridor") && !type.equals("neutral"))
                canvas.drawBitmap(bIcon, getCenterX() - (bIcon.getWidth() / 2), getCenterY() - (bIcon.getHeight() / 2), icon);
        }

        IMuseumsSQLiteHelper sqLiteHelper = IMuseumsSQLiteHelper.getInstance(context);

        SQLiteDatabase database = sqLiteHelper.getWritableDatabase();
        Cursor c = database.rawQuery("SELECT * FROM VisitState WHERE museum='" + museum + "' AND area='" + area + "' AND room='" + name + "'", null);

        if (actual) {
            Bitmap bIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.actual_room);
            canvas.drawBitmap(bIcon, getOnScreenX(), getOnScreenY(), new Paint());

            if (c.getCount() > 0) {
                database.execSQL("UPDATE VisitState SET state=" + 0 + " WHERE museum='" + museum + "' AND area='" + area + "' AND room='" + name + "'");
            } else {
                database.execSQL("INSERT INTO VisitState (museum, area, room, state) VALUES ('" + museum + "', '" + area + "', '" + name + "', " + 0 + ")");
            }
        } else if (visited) {
            Bitmap bIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.visited_room);
            canvas.drawBitmap(bIcon, getOnScreenX(), getOnScreenY(), new Paint());

            if (c.getCount() > 0) {
                database.execSQL("UPDATE VisitState SET state=" + 1 + " WHERE museum='" + museum + "' AND area='" + area + "' AND room='" + name + "'");
            } else {
                database.execSQL("INSERT INTO VisitState (museum, area, room, state) VALUES ('" + museum + "', '" + area + "', '" + name + "', " + 1 + ")");
            }
        } else {
            if (c.getCount() > 0) {
                database.execSQL("UPDATE VisitState SET state=" + 2 + " WHERE museum='" + museum + "' AND area='" + area + "' AND room='" + name + "'");
            } else {
                database.execSQL("INSERT INTO VisitState (museum, area, room, state) VALUES ('" + museum + "', '" + area + "', '" + name + "', " + 2 + ")");
            }
        }

        for (Space space : spaces) {
            space.draw(context, canvas, ratioWidth, ratioHeight);
        }

        return canvas;
    }

    public float getCenterX() {
        return getOnScreenX() + (getOnScreenWidth() / 2);
    }

    public float getCenterY() {
        return getOnScreenY() + (getOnScreenHeight() / 2);
    }

    public Door getDoorByDestiny(String destinyRoom) {
        for (Space space : spaces) {
            Door result = space.getDoorByDestinyRoom(destinyRoom);

            if (result != null)
                return result;
        }
        return null;
    }

    public boolean select(float pointedX, float pointedY, float x, float y) {

        float startX = getOnScreenX() + x;
        float endX = getOnScreenX() + getOnScreenWidth() + x;
        float startY = getOnScreenY() + y;
        float endY = getOnScreenY() + getOnScreenHeight() + y;

        String result = (startX <= pointedX && endX >= pointedX) && (startY <= pointedY && endY >= pointedY) ? name.get(Locale.getDefault().getLanguage()) : null;

        if (null != result) {
            bgColor = Color.argb(255, 255, 255, 0);
            visited = false;
            actual = true;
        } else {
            bgColor = Color.argb(255, 255, 255, 125);
            if (actual) {
                visited = true;
                actual = false;
            }
        }

        return null != result;
    }

    public void unSelect() {
        bgColor = Color.argb(255, 255, 255, 125);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name.get("es"));
        parcel.writeString(name.get("ca"));
        parcel.writeString(name.get("en"));
        parcel.writeString(type);
        parcel.writeString(descriptionPath.get("es"));
        parcel.writeString(descriptionPath.get("ca"));
        parcel.writeString(descriptionPath.get("en"));
        parcel.writeTypedList(spaces);
    }

    public static final Creator<Room> CREATOR = new Creator<Room>() {
        @Override
        public Room createFromParcel(Parcel parcel) {
            return new Room(parcel);
        }

        @Override
        public Room[] newArray(int i) {
            return new Room[i];
        }
    };

    public void readFromParcel(Parcel in) {
        name = new HashMap<String, String>();
        name.put("es", in.readString());
        name.put("ca", in.readString());
        name.put("en", in.readString());
        type = in.readString();
        descriptionPath = new HashMap<String, String>();
        descriptionPath.put("es", in.readString());
        descriptionPath.put("ca", in.readString());
        descriptionPath.put("en", in.readString());
        in.readTypedList(spaces, Space.CREATOR);
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setMuseum(String museum) {
        this.museum = museum;
    }
}
