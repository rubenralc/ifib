package com.rubenralc.ifib.app.model;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by ruben.rodriguez on 23/06/13.
 */
public class Space implements Parcelable {
    private int order;
    private ArrayList<Artwork> artworks;
    private ArrayList<Door> doors;

    public Space() {

    }

    public Space(Parcel in) {
        artworks = new ArrayList<Artwork>();
        doors = new ArrayList<Door>();
        readFromParcel(in);
    }

    public void readFromParcel(Parcel in) {
        order = in.readInt();
        in.readTypedList(artworks, Artwork.CREATOR);
        in.readTypedList(doors, Door.CREATOR);
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public ArrayList<Artwork> getArtworks() {
        return artworks;
    }

    public void setArtworks(ArrayList<Artwork> artworks) {
        this.artworks = artworks;
    }

    public ArrayList<Door> getDoors() {
        return doors;
    }

    public void setDoors(ArrayList<Door> doors) {
        this.doors = doors;
    }

    public Canvas draw(Context context, Canvas canvas, float ratioWidth, float ratioHeight) {
        for (Door door : doors) {
            door.setRatioWidth(ratioWidth);
            door.setRatioHeight(ratioHeight);
            canvas = door.draw(context, canvas);
        }
        return canvas;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(order);
        parcel.writeTypedList(artworks);
        parcel.writeTypedList(doors);
    }

    public static final Creator<Space> CREATOR = new Creator<Space>() {
        @Override
        public Space createFromParcel(Parcel parcel) {
            return new Space(parcel);
        }

        @Override
        public Space[] newArray(int i) {
            return new Space[i];
        }
    };

    public Door getDoorByDestinyRoom(String destinyRoom) {
        for (Door door : doors) {
            if(door.getDestinyRoom().equals(destinyRoom))
                return door;
        }
        return null;
    }
}
