package com.rubenralc.ifib.app;

import android.os.Bundle;
import android.os.Environment;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import org.ispeech.SpeechSynthesis;
import org.ispeech.SpeechSynthesisEvent;
import org.ispeech.error.BusyException;
import org.ispeech.error.InvalidApiKeyException;
import org.ispeech.error.NoNetworkException;

import java.util.HashMap;

/**
 * Created by Ruben on 1/07/13.
 */
public class ArtworkDescription extends MuseumsActivity {

    private WebView artworkDescription;
    private ImageView readIt;

    private SpeechSynthesis synthesis;

    private String artworkId;
    private String description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.artwork_description);

        museum = getIntent().getStringExtra("museum");
        shoppingCart = getIntent().getBooleanExtra("shoppingcart", false);
        artworkId = getIntent().getStringExtra("artwork");
        description = getIntent().getStringExtra("description");

        artworkDescription = (WebView) findViewById(R.id.artwork_description);
        readIt = (ImageView) findViewById(R.id.read_it);

        final String language = getResources().getConfiguration().locale.getLanguage();

        final TextExtractor textExtractor = new TextExtractor();
        artworkDescription.getSettings().setJavaScriptEnabled(true);
        artworkDescription.addJavascriptInterface(textExtractor, "iMuseums");
        artworkDescription.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                view.loadUrl("javascript:window.iMuseums.extractText(document.getElementsByTagName('body')[0].innerText);");
            }
        });
        artworkDescription.loadUrl("file://" + Environment.getExternalStorageDirectory() + "/imuseums/" + museum + "/" + artworkId + "/" + description);

        try {
            synthesis = SpeechSynthesis.getInstance(this);
            if ("es".equals(language))
                synthesis.setVoiceType("usspanishfemale");
            else if ("ca".equals(language))
                synthesis.setVoiceType("eurcatalanfemale");
            else
                synthesis.setVoiceType("ukenglishfemale");

            synthesis.setSpeechSynthesisEvent(new SpeechSynthesisEvent() {
                @Override
                public void onPlaySuccessful() {
                    readIt.setImageResource(R.drawable.read);
                    super.onPlaySuccessful();
                }

                @Override
                public void onPlayStopped() {
                    readIt.setImageResource(R.drawable.read);
                    super.onPlayStopped();
                }

                @Override
                public void onPlayFailed(Exception e) {
                    readIt.setImageResource(R.drawable.read);
                    super.onPlayFailed(e);
                }

                @Override
                public void onPlayCanceled() {
                    readIt.setImageResource(R.drawable.read);
                    super.onPlayCanceled();
                }
            });
        } catch (InvalidApiKeyException e) {
            e.printStackTrace();
        }

        readIt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (synthesis != null) {
                    if (textExtractor.getContent() != null) {
                        if (!synthesis.getTTSEngine().isSpeaking()) {
                            HashMap<String, String> ttsParams = new HashMap<String, String>();
                            ttsParams.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "done");
                            try {
                                String textToRead = textExtractor.getContent();
                                synthesis.speak(textToRead);
                            } catch (BusyException e) {
                                e.printStackTrace();
                            } catch (NoNetworkException e) {
                                e.printStackTrace();
                            }
                            readIt.setImageResource(R.drawable.pause_read);
                        } else {
                            synthesis.stop();
                            readIt.setImageResource(R.drawable.read);
                        }
                    }
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        if (synthesis != null && synthesis.getTTSEngine().isSpeaking()) {
            synthesis.stop();
        }
        super.onDestroy();
    }

    public class TextExtractor {
        private String content;

        public String getContent() {
            return content;
        }

        public void extractText(String content) {
            this.content = content;
        }
    }
}
