package com.rubenralc.ifib.app;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.rubenralc.ifib.app.listeners.CloseFragmentListener;

import java.util.Locale;

/**
 * Created by ruben.rodriguez on 21/08/13.
 */
public class MuseumsActivity extends FragmentActivity implements CloseFragmentListener {

    protected String museum;
    protected boolean shoppingCart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadPreferences();
        shoppingCart = getIntent().getBooleanExtra("shoppingcart", false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadPreferences();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent intent = new Intent(getBaseContext(), Settings.class);
                startActivityForResult(intent, 0);
                break;
            case R.id.go_shopping:
                Intent goToShoppingCart = new Intent(getBaseContext(), ShoppingCart.class);
                Bundle passParams = new Bundle();
                passParams.putString("museum", museum);
                passParams.putBoolean("shoppingcart", shoppingCart);
                goToShoppingCart.putExtras(passParams);
                startActivityForResult(goToShoppingCart, 0);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        this.recreate();
    }

    private void loadPreferences() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String language = sharedPreferences.getString("language_list", "es");
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("language_list", language);
        editor.commit();
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration configuration = new Configuration();
        configuration.locale = locale;
        getResources().updateConfiguration(configuration, null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.generic_menu, menu);
        MenuItem goShopping = menu.findItem(R.id.go_shopping);
        if(goShopping != null) goShopping.setVisible(shoppingCart);
        MenuItem addShoppingCart = menu.findItem(R.id.add_shopping);
        if(addShoppingCart != null) addShoppingCart.setVisible(shoppingCart);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onCloseFragment(String tag) {

    }
}
