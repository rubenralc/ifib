package com.rubenralc.ifib.app;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.rubenralc.ifib.app.adapters.AreaAdapter;
import com.rubenralc.ifib.app.fragments.RoomDescriptionFragment;
import com.rubenralc.ifib.app.model.Area;
import com.rubenralc.ifib.app.model.Room;
import com.rubenralc.ifib.app.model.Route;
import com.rubenralc.ifib.app.model.Step;
import com.rubenralc.ifib.app.utils.JsonHelper;
import com.rubenralc.ifib.app.views.AreaView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by ruben.rodriguez on 26/08/13.
 */
public class MapRoomSelection extends MuseumsActivity {

    private Spinner areaSelector;
    private AreaView areaView;

    private RoomDescriptionFragment roomDescriptionFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_room_selection);

        getActionBar().setDisplayHomeAsUpEnabled(true);

        areaSelector = (Spinner) findViewById(R.id.area_selector);
        areaView = (AreaView) findViewById(R.id.map_rooms);

        Bundle params = getIntent().getExtras();
        museum = params.getString("museum");
        shoppingCart = getIntent().getBooleanExtra("shoppingcart", false);
        areaView.setMuseum(museum);
        final String museum = params.getString("museum");
        final Route route = params.getParcelable("route");

        try {
            JsonHelper jsonHelper = new JsonHelper(getAssets());

            ArrayList<Area> areas = jsonHelper.getAreas(museum);
            Collections.sort(areas, new Comparator<Area>() {
                @Override
                public int compare(Area lhs, Area rhs) {
                    return lhs.getId().compareTo(rhs.getId());
                }
            });
            AreaAdapter areaAdapter = new AreaAdapter(getBaseContext(), areas);

            areaSelector.setAdapter(areaAdapter);
            areaSelector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    areaView.setArea((Area) adapterView.getSelectedItem());
                    areaView.setSteps(route.getSteps());
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            areaView.setOnTouchListener(new View.OnTouchListener() {

                private float startX = 0;
                private float startY = 0;

                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    switch (motionEvent.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            startX = motionEvent.getX();
                            startY = motionEvent.getY();
                            break;
                        case MotionEvent.ACTION_UP:
                            float pointedX = motionEvent.getX();
                            float pointedY = motionEvent.getY();

                            if (pointedX == startX && pointedY == startY) {

                                ArrayList<Room> selectedRooms = areaView.selectRooms(pointedX, pointedY);

                                Room selectedRoom;
                                if (selectedRooms != null && selectedRooms.size() > 0) {
                                    selectedRoom = selectedRooms.get(0);

                                    if (!selectedRoom.getType().equals("corridor")) {

                                        ArrayList<String> preferredArtworks = new ArrayList<String>();
                                        for (Step s : route.getSteps()) {
                                            if (s.getRoom() == selectedRoom.getId())
                                                preferredArtworks = s.getPreferredArtworks();
                                        }

                                        roomDescriptionFragment = new RoomDescriptionFragment(getBaseContext(), museum, selectedRoom, shoppingCart, preferredArtworks);
                                        getSupportFragmentManager().beginTransaction().add(R.id.fragment_placer, roomDescriptionFragment).commit();
                                    }
                                } else {
                                    onCloseFragment("");
                                }
                            }

                            break;
                    }
                    return areaView.onTouchEvent(motionEvent);
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public boolean onNavigateUp() {
        finish();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        onCloseFragment("");
    }

    @Override
    public void onCloseFragment(String tag) {
        if (roomDescriptionFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(roomDescriptionFragment).commit();
            roomDescriptionFragment = null;
            areaView.unSelectRooms();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK)
            if (roomDescriptionFragment != null)
                onCloseFragment("");
            else
                return super.onKeyDown(keyCode, event);
        return true;
    }
}
