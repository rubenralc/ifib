package com.rubenralc.ifib.app.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Ruben on 2/11/13.
 */
public class Element implements Parcelable {
    protected String id;
    protected String img;
    protected String type;

    protected Element() {
    }

    protected Element(Parcel in) {
        readFromParcel(in);
    }

    public void readFromParcel(Parcel in) {
        id = in.readString();
        img = in.readString();
        type=in.readString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(id);
        parcel.writeString(img);
        parcel.writeString(type);
    }

    public static final Creator<Element> CREATOR = new Creator<Element>() {
        @Override
        public Element createFromParcel(Parcel parcel) {
            return new Element(parcel);
        }

        @Override
        public Element[] newArray(int i) {
            return new Element[i];
        }
    };
}
