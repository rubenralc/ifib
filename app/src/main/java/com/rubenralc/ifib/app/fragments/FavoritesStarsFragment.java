package com.rubenralc.ifib.app.fragments;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.rubenralc.ifib.app.R;
import com.rubenralc.ifib.app.database.IMuseumsSQLiteHelper;
import com.rubenralc.ifib.app.listeners.CloseFragmentListener;
import com.rubenralc.ifib.app.model.Artwork;

/**
 * Created by Ruben on 2/01/14.
 */
public class FavoritesStarsFragment extends Fragment {

    private Context context;
    private String museum;
    private Artwork artwork;

    private RelativeLayout backgroundFragment;
    private ImageView pointOneStar;
    private ImageView pointTwoStar;
    private ImageView pointThreeStar;
    private ImageView pointFourStar;
    private ImageView pointFiveStar;

    private IMuseumsSQLiteHelper iMuseumsSQLiteHelper;

    private CloseFragmentListener closeFragmentListener;

    public FavoritesStarsFragment(Context context, String museum, Artwork artwork) {
        this.context = context;
        this.museum = museum;
        this.artwork = artwork;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.favorites_stars_fragment, container, false);

        backgroundFragment = (RelativeLayout) view.findViewById(R.id.background_fragment);
        pointOneStar = (ImageView) view.findViewById(R.id.point_one_star);
        pointTwoStar = (ImageView) view.findViewById(R.id.point_two_star);
        pointThreeStar = (ImageView) view.findViewById(R.id.point_three_star);
        pointFourStar = (ImageView) view.findViewById(R.id.point_four_star);
        pointFiveStar = (ImageView) view.findViewById(R.id.point_five_star);

        iMuseumsSQLiteHelper = IMuseumsSQLiteHelper.getInstance(context);

        SQLiteDatabase db = iMuseumsSQLiteHelper.getWritableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM Favorites WHERE id='" + artwork.getId() + "' AND museum='" + museum + "'", null);

        if (c.moveToFirst()) {
            int points = c.getInt(c.getColumnIndex("points"));

            switch (points) {
                case 5:
                    pointFiveStar.setImageDrawable(getResources().getDrawable(R.drawable.star_fav_full));
                case 4:
                    pointFourStar.setImageDrawable(getResources().getDrawable(R.drawable.star_fav_full));
                case 3:
                    pointThreeStar.setImageDrawable(getResources().getDrawable(R.drawable.star_fav_full));
                case 2:
                    pointTwoStar.setImageDrawable(getResources().getDrawable(R.drawable.star_fav_full));
                case 1:
                    pointOneStar.setImageDrawable(getResources().getDrawable(R.drawable.star_fav_full));
                    break;
            }
        }

        backgroundFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeFragmentListener.onCloseFragment(getTag());
            }
        });

        pointOneStar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLiteDatabase db = iMuseumsSQLiteHelper.getWritableDatabase();
                Cursor c = db.rawQuery("SELECT * FROM Favorites WHERE id='" + artwork.getId() + "' AND museum='" + museum + "'", null);

                if (c.getCount() < 1) {
                    db.execSQL("INSERT INTO Favorites (id, name, img, museum, points) VALUES ('" + artwork.getId() + "','" + artwork.getName("es") + "','" + artwork.getImg() + "', '" + museum + "'," + 1 + ")");
                } else {
                    db.execSQL("UPDATE Favorites set points=" + 1 + " WHERE id='" + artwork.getId() + "' AND museum='" + museum + "'");
                }
                closeFragmentListener.onCloseFragment(getTag());
            }
        });

        pointTwoStar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLiteDatabase db = iMuseumsSQLiteHelper.getWritableDatabase();
                Cursor c = db.rawQuery("SELECT * FROM Favorites WHERE id='" + artwork.getId() + "' AND museum='" + museum + "'", null);

                if (c.getCount() < 1) {
                    db.execSQL("INSERT INTO Favorites (id, name, img, museum, points) VALUES ('" + artwork.getId() + "','" + artwork.getName("es") + "','" + artwork.getImg() + "', '" + museum + "'," + 2 + ")");
                } else {
                    db.execSQL("UPDATE Favorites set points=" + 2 + " WHERE id='" + artwork.getId() + "' AND museum='" + museum + "'");
                }
                closeFragmentListener.onCloseFragment(getTag());
            }
        });

        pointThreeStar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLiteDatabase db = iMuseumsSQLiteHelper.getWritableDatabase();
                Cursor c = db.rawQuery("SELECT * FROM Favorites WHERE id='" + artwork.getId() + "' AND museum='" + museum + "'", null);

                if (c.getCount() < 1) {
                    db.execSQL("INSERT INTO Favorites (id, name, img, museum, points) VALUES ('" + artwork.getId() + "','" + artwork.getName("es") + "','" + artwork.getImg() + "', '" + museum + "'," + 3 + ")");
                } else {
                    db.execSQL("UPDATE Favorites set points=" + 3 + " WHERE id='" + artwork.getId() + "' AND museum='" + museum + "'");
                }
                closeFragmentListener.onCloseFragment(getTag());
            }
        });

        pointFourStar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLiteDatabase db = iMuseumsSQLiteHelper.getWritableDatabase();
                Cursor c = db.rawQuery("SELECT * FROM Favorites WHERE id='" + artwork.getId() + "' AND museum='" + museum + "'", null);

                if (c.getCount() < 1) {
                    db.execSQL("INSERT INTO Favorites (id, name, img, museum, points) VALUES ('" + artwork.getId() + "','" + artwork.getName("es") + "','" + artwork.getImg() + "', '" + museum + "'," + 4 + ")");
                } else {
                    db.execSQL("UPDATE Favorites set points=" + 4 + " WHERE id='" + artwork.getId() + " AND museum='" + museum + "''");
                }
                closeFragmentListener.onCloseFragment(getTag());
            }
        });

        pointFiveStar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLiteDatabase db = iMuseumsSQLiteHelper.getWritableDatabase();
                Cursor c = db.rawQuery("SELECT * FROM Favorites WHERE id='" + artwork.getId() + "' AND museum='" + museum + "'", null);

                if (c.getCount() < 1) {
                    db.execSQL("INSERT INTO Favorites (id, name, img, museum, points) VALUES ('" + artwork.getId() + "','" + artwork.getName("es") + "','" + artwork.getImg() + "', '" + museum + "'," + 5 + ")");
                } else {
                    db.execSQL("UPDATE Favorites set points=" + 5 + " WHERE id='" + artwork.getId() + "' AND museum='" + museum + "'");
                }
                closeFragmentListener.onCloseFragment(getTag());
            }
        });

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        closeFragmentListener = (CloseFragmentListener) activity;
    }
}
