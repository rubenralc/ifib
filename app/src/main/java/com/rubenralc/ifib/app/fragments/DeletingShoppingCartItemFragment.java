package com.rubenralc.ifib.app.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

/**
 * Created by Ruben on 27/12/13.
 */
public class DeletingShoppingCartItemFragment extends DialogFragment {

    public interface EventListener{
        public void deleteItem(DialogFragment dialog);
    }

    private EventListener listener;

    private String artworkName;

    public static DeletingShoppingCartItemFragment newInstance(String artworkName){
        DeletingShoppingCartItemFragment f = new DeletingShoppingCartItemFragment();

        Bundle args = new Bundle();
        args.putString("artworkName", artworkName);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (EventListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement EventListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        this.artworkName = getArguments().getString("artworkName");

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(String.format("¿Está seguro que desea eliminar %s del carrito?", artworkName))
                .setPositiveButton("Sí", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listener.deleteItem(DeletingShoppingCartItemFragment.this);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DeletingShoppingCartItemFragment.this.dismiss();
                    }
                });

        return builder.create();
    }
}
