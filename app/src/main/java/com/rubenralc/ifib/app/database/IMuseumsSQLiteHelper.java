package com.rubenralc.ifib.app.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Ruben on 17/01/14.
 */
public class IMuseumsSQLiteHelper extends SQLiteOpenHelper {

    String sqlCreateVisitState = "CREATE TABLE VisitState(museum VARCHAR(200), area VARCHAR(200), room VARCHAR(200), state INTEGER)";
    String sqlCreateShoppingCartItems = "CREATE TABLE ShoppingCartItems(id VARCHAR(200), name VARCHAR(200), img VARCHAR(200), museum VARCHAR(200), quantity INTEGER)";
    String sqlCreateFavorites = "CREATE TABLE Favorites(id VARCHAR(200), name VARCHAR(200), img VARCHAR(200), museum VARCHAR(200), points INTEGER)";

    private static IMuseumsSQLiteHelper instance;

    public static IMuseumsSQLiteHelper getInstance(Context context){
        if(instance == null)
            instance = new IMuseumsSQLiteHelper(context, "imuseumsDB", null, 6);

        return instance;
    }

    private IMuseumsSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlCreateFavorites);
        db.execSQL(sqlCreateShoppingCartItems);
        db.execSQL(sqlCreateVisitState);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS Favorites");
        db.execSQL(sqlCreateFavorites);
        db.execSQL("DROP TABLE IF EXISTS ShoppingCartItems");
        db.execSQL(sqlCreateShoppingCartItems);
        db.execSQL("DROP TABLE IF EXISTS VisitState");
        db.execSQL(sqlCreateVisitState);
    }
}
