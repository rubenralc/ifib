package com.rubenralc.ifib.app.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Ruben on 26/11/13.
 */
public class ShoppingCartItemsSQLiteHelper extends SQLiteOpenHelper {

    String sqlCreate = "CREATE TABLE ShoppingCartItems(id VARCHAR(200), name VARCHAR(200), img VARCHAR(200), museum VARCHAR(200), quantity INTEGER)";

    private static ShoppingCartItemsSQLiteHelper instance;

    public static  ShoppingCartItemsSQLiteHelper getInstance(Context context){
        if(instance != null)
            return instance;

        return new ShoppingCartItemsSQLiteHelper(context, "imuseumsDB", null, 5);
    }

    private ShoppingCartItemsSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory cursorFactory, int version) {
        super(context, name, cursorFactory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlCreate);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS ShoppingCartItems");
        db.execSQL(sqlCreate);
    }
}
