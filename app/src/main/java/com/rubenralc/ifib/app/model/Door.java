package com.rubenralc.ifib.app.model;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Parcel;

import com.rubenralc.ifib.app.model.interfaces.IDrawable;
import com.rubenralc.ifib.app.utils.Constants;

/**
 * Created by Ruben on 2/11/13.
 */
public class Door extends Element implements IDrawable {

    private int x;
    private int y;
    private int width;
    private String direction;
    private String destinyRoom;
    private float ratioWidth;
    private float ratioHeight;

    public Door() {
        super();
    }

    public Door(Parcel in) {
        super(in);
    }

    public void readFromParcel(Parcel in) {
        super.readFromParcel(in);
        x = in.readInt();
        y = in.readInt();
        width = in.readInt();
        direction = in.readString();
        destinyRoom = in.readString();
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getDestinyRoom() {
        return destinyRoom;
    }

    public void setDestinyRoom(String destinyRoom) {
        this.destinyRoom = destinyRoom;
    }

    public float getOnScreenX() {
        return x * ratioWidth + 16;
    }

    public float getOnScreenY() {
        return y * ratioHeight + 16;
    }

    public float getOnScreenWidth() {
        return width * ratioWidth;
    }

    public float getCenterX() {
        if (direction.equals(Constants.HORI)) {
            return getOnScreenX() + (getOnScreenWidth() / 2);
        }
        return getOnScreenX();
    }

    public float getCenterY() {
        if (direction.equals(Constants.VERT)) {
            return getOnScreenY() + (getOnScreenWidth() / 2);
        }
        return getOnScreenY();
    }

    public void setRatioWidth(float ratioWidth) {
        this.ratioWidth = ratioWidth;
    }

    public void setRatioHeight(float ratioHeight) {
        this.ratioHeight = ratioHeight;
    }

    @Override
    public Canvas draw(Context context, Canvas canvas) {
        Paint door = new Paint();
        door.setStrokeWidth(5);

        if (direction.equals(Constants.VERT)) {
            canvas.drawLine(getOnScreenX(), getOnScreenY(), getOnScreenX(), getOnScreenY() + getOnScreenWidth(), door);
        } else {
            canvas.drawLine(getOnScreenX(), getOnScreenY(), getOnScreenX() + getOnScreenWidth(), getOnScreenY(), door);
        }
        return canvas;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeInt(x);
        out.writeInt(y);
        out.writeInt(width);
        out.writeString(direction);
        out.writeString(destinyRoom);
    }

    public static final Creator<Door> CREATOR = new Creator<Door>() {
        @Override
        public Door createFromParcel(Parcel parcel) {
            return new Door(parcel);
        }

        @Override
        public Door[] newArray(int i) {
            return new Door[i];
        }
    };
}
