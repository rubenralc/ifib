package com.rubenralc.ifib.app.model;

/**
 * Created by Ruben on 16/06/13.
 */
public class Sponsor {
    private String name;
    private String img;
    private String url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
