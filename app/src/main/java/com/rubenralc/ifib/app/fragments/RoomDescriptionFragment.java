package com.rubenralc.ifib.app.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;

import com.rubenralc.ifib.app.R;
import com.rubenralc.ifib.app.RoomDetail;
import com.rubenralc.ifib.app.listeners.CloseFragmentListener;
import com.rubenralc.ifib.app.model.Room;

import java.util.ArrayList;

/**
 * Created by ruben.rodriguez on 29/08/13.
 */
public class RoomDescriptionFragment extends Fragment {

    private Context context;
    private String museum;
    private String descriptionPath;
    private Room room;
    private Boolean shoppingCart;
    private ArrayList<String> preferredArtworks;

    private WebView roomDescription;
    private Button enter;
    private Button close;

    private CloseFragmentListener closeFragmentListener;

    public RoomDescriptionFragment(Context context, String museum, Room room, Boolean shoppingCart, ArrayList<String> preferredArtworks) {
        this.context = context;
        this.museum = museum;
        this.shoppingCart = shoppingCart;
        this.descriptionPath = room.getDescriptionPath(context.getResources().getConfiguration().locale.getLanguage());
        this.room = room;
        this.preferredArtworks = preferredArtworks;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.room_description_fragment, container, false);

        roomDescription = (WebView) v.findViewById(R.id.room_description);
        enter = (Button) v.findViewById(R.id.enter_room);
        close = (Button) v.findViewById(R.id.close_room);

        if (!room.getType().equals("artwork")) {
            enter.setVisibility(View.INVISIBLE);
        }

        loadContent();

        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        closeFragmentListener = (CloseFragmentListener) activity;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadContent();
    }

    private void loadContent(){
        String language = context.getResources().getConfiguration().locale.getLanguage();

        roomDescription.loadUrl("file:///android_asset/" + museum + "/" + descriptionPath);

        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent goToRoom = new Intent(view.getContext(), RoomDetail.class);
                Bundle passParams = new Bundle();
                passParams.putParcelable("room", room);
                passParams.putString("museum", museum);
                passParams.putBoolean("shoppingcart", shoppingCart);
                passParams.putStringArrayList("preferredartworks", preferredArtworks);
                goToRoom.putExtras(passParams);
                startActivityForResult(goToRoom, 0);
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeFragmentListener.onCloseFragment(getTag());
            }
        });
    }
}
