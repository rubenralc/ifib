package com.rubenralc.ifib.app;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;

import com.rubenralc.ifib.app.adapters.AreaAdapter;
import com.rubenralc.ifib.app.adapters.RoomAdapter;
import com.rubenralc.ifib.app.model.Area;
import com.rubenralc.ifib.app.model.Room;
import com.rubenralc.ifib.app.utils.JsonHelper;

import java.util.ArrayList;

/**
 * Created by Ruben on 18/06/13.
 */
public class RoomSelection extends MuseumsActivity {

    private Spinner areaSelector;
    private Spinner roomSelector;
    private WebView roomDescription;
    private Button enter;

    private Room room;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.room_selection);

        getActionBar().setDisplayHomeAsUpEnabled(true);

        areaSelector = (Spinner) findViewById(R.id.area_selector);
        roomSelector = (Spinner) findViewById(R.id.room_selector);
        roomDescription = (WebView) findViewById(R.id.room_description);
        enter = (Button) findViewById(R.id.enter_room);

        Bundle params = getIntent().getExtras();
        final String museum = params.getString("museum");
        String route = params.getString("route");

        try {
            JsonHelper jsonHelper = new JsonHelper(getAssets());

            ArrayList<Area> areas = jsonHelper.getAreas(museum);
            AreaAdapter areaAdapter = new AreaAdapter(getBaseContext(), areas);

            areaSelector.setAdapter(areaAdapter);
            areaSelector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    ArrayList<Room> rooms = ((Area) adapterView.getSelectedItem()).getRooms();
                    RoomAdapter roomAdapter = new RoomAdapter(getBaseContext(), rooms);

                    roomSelector.setAdapter(roomAdapter);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            roomSelector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    String descriptionPath = ((Room) adapterView.getSelectedItem()).getDescriptionPath(getResources().getConfiguration().locale.getLanguage());
                    room = (Room) adapterView.getSelectedItem();
                    roomDescription.loadUrl("file://" + Environment.getExternalStorageDirectory() + "/imuseums/" + museum + "/" + descriptionPath);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            enter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent goToRoom = new Intent(view.getContext(), RoomDetail.class);
                    Bundle passParams = new Bundle();
                    passParams.putParcelable("room", room);
                    passParams.putString("museum", museum);
                    goToRoom.putExtras(passParams);
                    startActivityForResult(goToRoom, 0);
                }
            });

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public boolean onNavigateUp() {
        finish();
        return true;
    }
}
