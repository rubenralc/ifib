package com.rubenralc.ifib.app.model;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Canvas;

import com.rubenralc.ifib.app.database.IMuseumsSQLiteHelper;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Ruben on 19/06/13.
 */
public class Area {
    private String id;
    private String name;
    private ArrayList<Room> rooms;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Room> getRooms() {
        return rooms;
    }

    public void setRooms(ArrayList<Room> rooms) {
        this.rooms = rooms;
    }

    public Canvas draw(Context context, Canvas canvas, float ratioWidth, float ratioHeight, String museum) {
        for (Room room : rooms) {
            room.setRatioWidth(ratioWidth);
            room.setRatioHeight(ratioHeight);
            room.setMuseum(museum);
            room.setArea(id);
            canvas = room.draw(context, canvas);
        }
        return canvas;
    }

    public ArrayList<Room> select(float pointedX, float pointedY, float x, float y) {
        ArrayList<Room> selectedRooms = new ArrayList<Room>();

        for (Room room : rooms) {
            if (room.select(pointedX, pointedY, x, y))
                selectedRooms.add(room);
        }

        return selectedRooms;
    }

    public void unSelect() {
        for (Room room : rooms) {
            room.unSelect();
        }
    }

    public int getMaxWidth() {
        int result = 0;
        for (Room r : rooms) {
            if (result < r.getX() + r.getWidth()) result = r.getX() + r.getWidth();
        }
        return result;
    }

    public int getMaxHeight() {
        int result = 0;
        for (Room r : rooms) {
            if (result < r.getY() + r.getHeight()) result = r.getY() + r.getHeight();
        }
        return result;
    }

    public Room getRoom(String room) {
        for (Room r : rooms) {
            if (r.getName(Locale.getDefault().getLanguage()).equals(room))
                return r;
        }
        return null;
    }

    public Room getRoom(String area, String room) {
        if (!id.equals(area)) {
            return null;
        }

        Room result = null;

        for (Room r : rooms) {
            if (r.getName(Locale.getDefault().getLanguage()).equals(room))
                result = r;
        }

        return result;
    }

    public void setState(Context context, String museum) {
        IMuseumsSQLiteHelper sqLiteHelper = IMuseumsSQLiteHelper.getInstance(context);

        SQLiteDatabase database = sqLiteHelper.getReadableDatabase();

        for (Room r : rooms) {
            Cursor c = database.rawQuery("SELECT * FROM VisitState WHERE museum='" + museum + "' AND area='" + this.id + "' AND room='" + r.getName(Locale.getDefault().getLanguage()) + "'", null);

            if (c.getCount() > 0) {
                c.moveToFirst();

                String value = c.getString(c.getColumnIndex("state"));

                if ("0".equals(value)) {
                    r.setActual(true);
                    r.setVisited(false);
                } else if ("1".equals(value)) {
                    r.setActual(false);
                    r.setVisited(true);
                } else if ("2".equals(value)) {
                    r.setActual(false);
                    r.setVisited(false);
                }
            }
        }
    }
}
