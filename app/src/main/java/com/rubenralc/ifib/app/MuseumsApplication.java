package com.rubenralc.ifib.app;

import android.app.Application;

import com.rubenralc.ifib.app.model.Route;

/**
 * Created by Ruben on 07/04/2014.
 */
public class MuseumsApplication extends Application {

    private static MuseumsApplication instance;

    public MuseumsApplication getInstance(){
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    private Route selectedRoute;

    public void setSelectedRoute(Route route){
        selectedRoute = route;
    }

    public Route getSelectedRoute(){
        return selectedRoute;
    }
}
