package com.rubenralc.ifib.app.utils;

import android.content.res.AssetManager;
import android.os.Environment;

import com.rubenralc.ifib.app.model.Area;
import com.rubenralc.ifib.app.model.Artwork;
import com.rubenralc.ifib.app.model.Door;
import com.rubenralc.ifib.app.model.Museum;
import com.rubenralc.ifib.app.model.Room;
import com.rubenralc.ifib.app.model.Route;
import com.rubenralc.ifib.app.model.Space;
import com.rubenralc.ifib.app.model.Sponsor;
import com.rubenralc.ifib.app.model.Step;
import com.rubenralc.ifib.app.model.Video;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by Ruben on 8/06/13.
 */
public class JsonHelper {

    private InputStreamReader museumsFile = null;
    private InputStreamReader museumFile = null;
    private InputStreamReader routeFile = null;
    private InputStreamReader stepFile = null;
    private AssetManager assets = null;

    public JsonHelper(AssetManager assets) throws IOException {
        this.assets = assets;
        museumsFile = new InputStreamReader(assets.open("museums.json"));

    }

    public ArrayList<Museum> getMuseums() {

        ArrayList<Museum> museums = new ArrayList<Museum>();

        try {
            JSONObject doc;
            JSONParser parser = new JSONParser();

            doc = (JSONObject) parser.parse(museumsFile);

            JSONArray array = (JSONArray) doc.get("museums");

            for (Object m : array) {
                JSONObject jsonMuseum = (JSONObject) m;

                Museum museum = new Museum();
                museum.setName((String) jsonMuseum.get("name"));
                museum.setLargeName((String) jsonMuseum.get("large_name"));
                museum.setImg((String) jsonMuseum.get("img"));

                JSONObject jsonDescription = (JSONObject) jsonMuseum.get("description");
                museum.setDescription("es", (String) jsonDescription.get("es"));
                museum.setDescription("ca", (String) jsonDescription.get("ca"));
                museum.setDescription("en", (String) jsonDescription.get("en"));

                museum.setShoppingCart("yes".equals(jsonMuseum.get("shoppingcart")));

                JSONObject jsonSponsorsLiteral = (JSONObject) jsonMuseum.get("sponsorsliteral");
                museum.setSponsorsLiteral("es", (String) jsonSponsorsLiteral.get("es"));
                museum.setSponsorsLiteral("ca", (String) jsonSponsorsLiteral.get("ca"));
                museum.setSponsorsLiteral("en", (String) jsonSponsorsLiteral.get("en"));

                museums.add(museum);
            }
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } catch (ParseException parseException) {
            parseException.printStackTrace();
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return museums;
    }

    public ArrayList<Route> getRoutes(String museum) throws IOException {
        museumFile = new InputStreamReader(assets.open(museum + "/" + museum + ".json"));

        ArrayList<Route> routes = new ArrayList<Route>();

        try {
            JSONObject doc;
            JSONParser parser = new JSONParser();

            doc = (JSONObject) parser.parse(museumFile);

            JSONArray array = (JSONArray) doc.get("routes");

            for (Object m : array) {
                JSONObject r = (JSONObject) m;
                Route route = new Route();
                route.setId((String) r.get("id"));
                JSONObject names = (JSONObject) r.get("name");
                route.setName("es", names.get("es").toString());
                route.setName("ca", names.get("ca").toString());
                route.setName("en", names.get("en").toString());
                routeFile = new InputStreamReader(assets.open(museum + "/" + route.getId() + ".json"));
                JSONObject docSteps;
                JSONParser parserSteps = new JSONParser();

                docSteps = (JSONObject) parserSteps.parse(routeFile);

                JSONArray arraySteps = (JSONArray) docSteps.get("steps");

                ArrayList<Step> steps = new ArrayList<Step>();
                for (Object s : arraySteps) {
                    JSONObject jsonStep = (JSONObject) s;
                    Step step = new Step();
                    step.setArea((String) jsonStep.get("area"));
                    step.setRoom((String) jsonStep.get("room"));
                    JSONArray jsonPreferredArtworks = (JSONArray) jsonStep.get("preferred_artworks");
                    ArrayList<String> preferredArtworks = new ArrayList<String>();
                    for (Object pa : jsonPreferredArtworks) {
                        preferredArtworks.add((String) pa);
                    }
                    step.setPreferredArtworks(preferredArtworks);
                    steps.add(step);
                }
                route.setSteps(steps);

                routes.add(route);
            }
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } catch (ParseException parseException) {
            parseException.printStackTrace();
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return routes;
    }

    public ArrayList<Sponsor> getSponsors(String museum) throws IOException {
        museumFile = new InputStreamReader(assets.open(museum + "/" + museum + ".json"));

        ArrayList<Sponsor> sponsors = new ArrayList<Sponsor>();

        try {
            JSONObject doc;
            JSONParser parser = new JSONParser();

            doc = (JSONObject) parser.parse(museumFile);

            JSONArray array = (JSONArray) doc.get("sponsors");

            for (Object m : array) {
                JSONObject jsonSponsor = (JSONObject) m;
                Sponsor sponsor = new Sponsor();
                sponsor.setName((String) jsonSponsor.get("name"));
                sponsor.setImg((String) jsonSponsor.get("img"));
                sponsor.setUrl((String) jsonSponsor.get("url"));
                sponsors.add(sponsor);
            }
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } catch (ParseException parseException) {
            parseException.printStackTrace();
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return sponsors;
    }

    public ArrayList<Video> getMuseumVideos(String museum) throws IOException {
        museumFile = new InputStreamReader(assets.open(museum + "/" + museum + ".json"));

        ArrayList<Video> videos = new ArrayList<Video>();

        try {
            JSONObject doc;
            JSONParser parser = new JSONParser();

            doc = (JSONObject) parser.parse(museumFile);

            JSONArray array = (JSONArray) doc.get("videos");

            for (Object m : array) {
                JSONObject jsonVideo = (JSONObject) m;
                Video video = new Video();

                JSONObject videoTitles = (JSONObject) jsonVideo.get("title");
                video.setTitle("es", videoTitles.get("es").toString());
                video.setTitle("ca", videoTitles.get("ca").toString());
                video.setTitle("en", videoTitles.get("en").toString());

                JSONObject videoDescriptions = (JSONObject) jsonVideo.get("description");
                video.setDescription("es", videoDescriptions.get("es").toString());
                video.setDescription("ca", videoDescriptions.get("ca").toString());
                video.setDescription("en", videoDescriptions.get("en").toString());

                video.setPath((String) jsonVideo.get("path"));
                video.setInternetStreaming((Boolean) jsonVideo.get("internet_streaming"));
                videos.add(video);
            }
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } catch (ParseException parseException) {
            parseException.printStackTrace();
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return videos;
    }

    public ArrayList<Area> getAreas(String museum) throws IOException {
        routeFile = new InputStreamReader(assets.open(museum + "/" + museum + ".json"));

        ArrayList<Area> areas = new ArrayList<Area>();
        try {
            JSONObject doc;
            JSONParser parser = new JSONParser();

            doc = (JSONObject) parser.parse(routeFile);

            JSONArray jsonAreas = (JSONArray) doc.get("areas");

            for (Object ar : jsonAreas) {
                JSONObject jsonArea = (JSONObject) ar;

                Area area = new Area();
                area.setId((String) jsonArea.get("id"));
                area.setName((String) jsonArea.get("name"));

                JSONArray array = (JSONArray) jsonArea.get("rooms");
                ArrayList<Room> rooms = new ArrayList<Room>();

                for (Object r : array) {
                    JSONObject jsonRoom = (JSONObject) r;

                    Room room = new Room();
                    room.setId((String) jsonRoom.get("id"));

                    JSONObject names = (JSONObject) jsonRoom.get("name");
                    room.setName("es", names.get("es").toString());
                    room.setName("ca", names.get("ca").toString());
                    room.setName("en", names.get("en").toString());

                    room.setType((String) jsonRoom.get("type"));

                    JSONObject descriptionPaths = (JSONObject) jsonRoom.get("description");
                    room.setDescriptionPath("es", descriptionPaths.get("es").toString());
                    room.setDescriptionPath("ca", descriptionPaths.get("ca").toString());
                    room.setDescriptionPath("en", descriptionPaths.get("en").toString());

                    room.setX(Integer.parseInt(jsonRoom.get("x").toString()));
                    room.setY(Integer.parseInt(jsonRoom.get("y").toString()));
                    room.setWidth(Integer.parseInt(jsonRoom.get("width").toString()));
                    room.setHeight(Integer.parseInt(jsonRoom.get("height").toString()));

                    JSONArray jsonSpaces = (JSONArray) jsonRoom.get("spaces");
                    ArrayList<Space> spaces = new ArrayList<Space>();
                    for (Object s : jsonSpaces) {
                        JSONObject jsonSpace = (JSONObject) s;
                        Space space = new Space();
                        space.setOrder(Integer.parseInt(jsonSpace.get("order").toString()));

                        JSONArray jsonElements = (JSONArray) jsonSpace.get("elements");
                        ArrayList<Artwork> artworks = new ArrayList<Artwork>();
                        ArrayList<Door> doors = new ArrayList<Door>();
                        for (Object a : jsonElements) {
                            JSONObject jsonElement = (JSONObject) a;

                            if (jsonElement.get("type").equals("artwork")) {
                                Artwork artwork = new Artwork();
                                artwork.setId((String) jsonElement.get("id"));
                                artwork.setType((String) jsonElement.get("type"));

                                JSONObject artworkNames = (JSONObject) jsonElement.get("name");
                                artwork.setName("es", artworkNames.get("es").toString());
                                artwork.setName("ca", artworkNames.get("ca").toString());
                                artwork.setName("en", artworkNames.get("en").toString());

                                artwork.setImg((String) jsonElement.get("img"));

                                JSONObject artworkTxts = (JSONObject) jsonElement.get("description");
                                artwork.setTxt("es", artworkTxts.get("es").toString());
                                artwork.setTxt("ca", artworkTxts.get("ca").toString());
                                artwork.setTxt("en", artworkTxts.get("en").toString());


                                JSONArray jsonVideos = (JSONArray) jsonElement.get("video");
                                ArrayList<Video> videos = new ArrayList<Video>();
                                for (Object v : jsonVideos) {
                                    JSONObject jsonVideo = (JSONObject) v;

                                    Video video = new Video();

                                    JSONObject videoTitles = (JSONObject) jsonVideo.get("title");
                                    video.setTitle("es", videoTitles.get("es").toString());
                                    video.setTitle("ca", videoTitles.get("ca").toString());
                                    video.setTitle("en", videoTitles.get("en").toString());

                                    JSONObject videoDescriptions = (JSONObject) jsonVideo.get("description");
                                    video.setDescription("es", videoDescriptions.get("es").toString());
                                    video.setDescription("ca", videoDescriptions.get("ca").toString());
                                    video.setDescription("en", videoDescriptions.get("en").toString());

                                    video.setPath((String) jsonVideo.get("path"));
                                    video.setInternetStreaming((Boolean) jsonVideo.get("internet_streaming"));

                                    videos.add(video);
                                }
                                artwork.setVideos(videos);
                                artworks.add(artwork);
                            } else if (jsonElement.get("type").equals("door")) {
                                Door door = new Door();
                                door.setId((String) jsonElement.get("id"));
                                door.setType((String) jsonElement.get("type"));
                                door.setImg((String) jsonElement.get("img"));
                                door.setX(Integer.parseInt(jsonElement.get("x").toString()));
                                door.setY(Integer.parseInt(jsonElement.get("y").toString()));
                                door.setWidth(Integer.parseInt(jsonElement.get("width").toString()));
                                door.setDirection((String) jsonElement.get("direction"));
                                door.setDestinyRoom((String) jsonElement.get("destiny_room"));
                                doors.add(door);
                            }
                        }
                        space.setArtworks(artworks);
                        space.setDoors(doors);
                        spaces.add(space);
                    }
                    room.setSpaces(spaces);
                    rooms.add(room);
                }
                area.setRooms(rooms);
                areas.add(area);
            }
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } catch (ParseException parseException) {
            parseException.printStackTrace();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return areas;
    }
}
