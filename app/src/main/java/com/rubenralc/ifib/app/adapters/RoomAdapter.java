package com.rubenralc.ifib.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.rubenralc.ifib.app.R;
import com.rubenralc.ifib.app.model.Room;

import java.util.ArrayList;

/**
 * Created by Ruben on 19/06/13.
 */
public class RoomAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Room> mRooms;

    public RoomAdapter(Context context, ArrayList<Room> rooms) {
        this.context = context;
        mRooms = rooms;
    }

    @Override
    public int getCount() {
        return mRooms.size();
    }

    @Override
    public Object getItem(int i) {
        return mRooms.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        RoomViewHolder roomViewHolder;

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.room_item, null);

            roomViewHolder = new RoomViewHolder();

            roomViewHolder.roomName = (TextView) view.findViewById(R.id.room_name);

            view.setTag(roomViewHolder);
        } else {
            roomViewHolder = (RoomViewHolder) view.getTag();
        }
        Room room = mRooms.get(i);
        if (room != null) {
            roomViewHolder.roomName.setText(room.getName(context.getResources().getConfiguration().locale.getLanguage()));
        }

        return view;
    }

    static class RoomViewHolder {
        TextView roomName;
    }
}
