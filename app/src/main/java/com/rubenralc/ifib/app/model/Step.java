package com.rubenralc.ifib.app.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Ruben on 17/10/13.
 */
public class Step implements Parcelable {
    private String area;
    private String room;
    private ArrayList<String> preferredArtworks;

    public Step() {

    }

    public Step(Parcel in) {
        readFromParcel(in);
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public ArrayList<String> getPreferredArtworks() {
        return preferredArtworks;
    }

    public void setPreferredArtworks(ArrayList<String> preferredArtworks) {
        this.preferredArtworks = preferredArtworks;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(area);
        out.writeString(room);
        out.writeStringList(preferredArtworks);
    }

    public static final Creator<Step> CREATOR = new Creator<Step>() {
        @Override
        public Step createFromParcel(Parcel parcel) {
            return new Step(parcel);
        }

        @Override
        public Step[] newArray(int i) {
            return new Step[i];
        }
    };

    public void readFromParcel(Parcel in) {
        area = in.readString();
        room = in.readString();
        preferredArtworks = new ArrayList<String>();
        in.readStringList(preferredArtworks);
    }
}
