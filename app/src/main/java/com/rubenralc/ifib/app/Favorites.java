package com.rubenralc.ifib.app;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.rubenralc.ifib.app.adapters.FavoriteItemAdapter;
import com.rubenralc.ifib.app.database.IMuseumsSQLiteHelper;
import com.rubenralc.ifib.app.fragments.DeletingShoppingCartItemFragment;
import com.rubenralc.ifib.app.model.FavoriteItem;
import com.rubenralc.ifib.app.model.ShoppingCartItem;

import java.util.ArrayList;

/**
 * Created by Ruben on 4/01/14.
 */
public class Favorites extends MuseumsActivity {

    private ListView listFavorites;

    private int selected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.favorites);

        getActionBar().setDisplayHomeAsUpEnabled(true);

        listFavorites = (ListView) findViewById(R.id.favorites_list);

        museum = getIntent().getStringExtra("museum");

        IMuseumsSQLiteHelper sqLiteHelper = IMuseumsSQLiteHelper.getInstance(this);

        SQLiteDatabase db = sqLiteHelper.getWritableDatabase();

        if (db != null) {

            Cursor c = db.rawQuery("SELECT * FROM Favorites WHERE museum='" + museum + "'", null);

            ArrayList<FavoriteItem> favoriteItems = new ArrayList<FavoriteItem>();

            if (c.moveToFirst()) {
                do {
                    FavoriteItem favoriteItem = new FavoriteItem();
                    favoriteItem.setId(c.getString(0));
                    favoriteItem.setName(c.getString(1));
                    favoriteItem.setImg(c.getString(2));
                    favoriteItem.setMuseum(museum);
                    favoriteItem.setPunctuation(Integer.parseInt(c.getString(4)));

                    favoriteItems.add(favoriteItem);
                } while (c.moveToNext());
            }

            listFavorites.setAdapter(new FavoriteItemAdapter(getBaseContext(), favoriteItems));
            listFavorites.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    selected = position;
                    DialogFragment dialogFragment = DeletingShoppingCartItemFragment.newInstance(((ShoppingCartItem) listFavorites.getItemAtPosition(selected)).getName());
                    dialogFragment.show(getSupportFragmentManager(), "deleteItem");
                }
            });
        }
    }

    @Override
    public boolean onNavigateUp() {
        finish();
        return true;
    }
}
