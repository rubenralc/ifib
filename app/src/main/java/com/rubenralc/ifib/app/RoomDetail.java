package com.rubenralc.ifib.app;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.MotionEvent;
import android.widget.TableLayout;

import com.rubenralc.ifib.app.fragments.SpaceFragment;
import com.rubenralc.ifib.app.model.Room;
import com.rubenralc.ifib.app.model.Route;
import com.rubenralc.ifib.app.model.Space;

import java.util.ArrayList;

/**
 * Created by Ruben on 19/06/13.
 */
public class RoomDetail extends MuseumsActivity {
    private TableLayout area;

    private Room room;
    private ArrayList<String> preferredArtworks;

    private int actualSpace = 0;
    private ArrayList<Space> spaces;

    private Route route;

    private RoomDetail act;
    private SpaceFragment spaceFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.room_detail);

        getActionBar().setDisplayHomeAsUpEnabled(true);

        act = this;

        Bundle params = getIntent().getExtras();
        room = params.getParcelable("room");
        museum = params.getString("museum");
        shoppingCart = getIntent().getBooleanExtra("shoppingcart", false);
        preferredArtworks = getIntent().getStringArrayListExtra("preferredartworks");

        spaces = room.getSpaces();

        MuseumsApplication museumsApplication = (MuseumsApplication) getApplication();
        route = museumsApplication.getSelectedRoute();

        spaceFragment = new SpaceFragment(getBaseContext(), this, museum, spaces.get(actualSpace).getArtworks(), spaces.get(actualSpace).getDoors(), shoppingCart, preferredArtworks, route);

        getFragmentManager().beginTransaction().replace(android.R.id.content, spaceFragment).commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onNavigateUp() {
        finish();
        return true;
    }

    float x1 = 0, x2 = 0, y1 = 0, y2 = 0, dx = 0, dy = 0;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case (MotionEvent.ACTION_DOWN):
                x1 = event.getX();
                y1 = event.getY();
                break;
            case (MotionEvent.ACTION_UP):
                x2 = event.getX();
                y2 = event.getY();
                dx = x2 - x1;
                dy = y2 - y1;

                // Use dx and dy to determine the direction
                if (Math.abs(dx) > Math.abs(dy)) {
                    if (dx > 0) prevSpace();
                    else nextSpace();
                }
        }
        return super.onTouchEvent(event);
    }

    private void nextSpace() {
        if (actualSpace < room.getSpaces().size() - 1) {
            actualSpace++;

            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.animator.slide_in_left, R.animator.slide_out_left);

            SpaceFragment spaceFragment = new SpaceFragment(getBaseContext(), this, museum, spaces.get(actualSpace).getArtworks(), spaces.get(actualSpace).getDoors(), shoppingCart, preferredArtworks, route);

            fragmentTransaction.replace(android.R.id.content, spaceFragment);

            fragmentTransaction.commit();
        }
    }

    private void prevSpace() {
        if (actualSpace > 0) {
            actualSpace--;

            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.animator.slide_in_right, R.animator.slide_out_right);

            SpaceFragment spaceFragment = new SpaceFragment(getBaseContext(), this, museum, spaces.get(actualSpace).getArtworks(), spaces.get(actualSpace).getDoors(), shoppingCart, preferredArtworks, route);

            fragmentTransaction.replace(android.R.id.content, spaceFragment);

            fragmentTransaction.commit();
        }
    }
}
