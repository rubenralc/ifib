package com.rubenralc.ifib.app.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Ruben on 8/07/13.
 */
public class Images {
    public static Drawable getImageDrawable(AssetManager assets, Resources resources, String pathImg) {
        InputStream is = null;

        try {
            is = assets.open(pathImg);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Bitmap bi = BitmapFactory.decodeStream(is);

        return new BitmapDrawable(resources, bi);
    }

    public static int convertToPixels(Context context, int nDP) {
        final float conversionScale = context.getResources().getDisplayMetrics().density;

        return (int) ((nDP * conversionScale) + 0.5f);

    }
}
