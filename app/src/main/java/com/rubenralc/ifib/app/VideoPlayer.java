package com.rubenralc.ifib.app;

import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.widget.MediaController;
import android.widget.VideoView;

import com.rubenralc.ifib.app.model.Video;

/**
 * Created by Ruben on 2/07/13.
 */
public class VideoPlayer extends MuseumsActivity {

    private VideoView videoPlayer;

    private Video video;
    private String artworkId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_player);

        videoPlayer = (VideoView) findViewById(R.id.player);

        video = getIntent().getParcelableExtra("video");
        museum = getIntent().getStringExtra("museum");
        artworkId = getIntent().getStringExtra("artwork");

        if (!video.isInternetStreaming()) {
            String videoPath = Environment.getExternalStorageDirectory() + "/imuseums/" + museum + "/" + artworkId + "/" + video.getPath();
            videoPlayer.setVideoPath(videoPath);
        } else {
            videoPlayer.setVideoURI(Uri.parse(video.getPath()));
        }

        videoPlayer.setMediaController(new MediaController(this));
        videoPlayer.requestFocus();
        videoPlayer.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        videoPlayer.stopPlayback();
    }
}
