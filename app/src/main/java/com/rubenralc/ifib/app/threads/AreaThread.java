package com.rubenralc.ifib.app.threads;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

import com.rubenralc.ifib.app.views.AreaView;

/**
 * Created by ruben.rodriguez on 26/08/13.
 */
public class AreaThread extends Thread {

    private SurfaceHolder sh;
    private AreaView areaView;
    private boolean run;

    public AreaThread(SurfaceHolder sh, AreaView areaView) {
        this.sh = sh;
        this.areaView = areaView;
        run = false;
    }

    public void setRunning(boolean run) {
        this.run = run;
    }

    @Override
    public void run() {
        run = true;
        while (run) {
            if (sh.getSurface().isValid()) {
                Canvas canvas = sh.lockCanvas(null);
                if (canvas != null)
                    areaView.draw(canvas);
            }
        }
    }
}
