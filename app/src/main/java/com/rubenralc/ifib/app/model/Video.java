package com.rubenralc.ifib.app.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

/**
 * Created by Ruben on 2/07/13.
 */
public class Video implements Parcelable {

    private HashMap<String, String> title;
    private HashMap<String, String> description;
    private String path;
    private boolean internetStreaming;

    public Video() {

    }

    public Video(Parcel in) {
        readFromParcel(in);
    }

    public String getTitle(String lang) {
        return title.get(lang);
    }

    public void setTitle(String title, String lang) {
        if (this.title == null)
            this.title = new HashMap<String, String>();
        this.title.put(lang, title);
    }

    public String getDescription(String lang) {
        return description.get(lang);
    }

    public void setDescription(String description, String lang) {
        if (this.description == null)
            this.description = new HashMap<String, String>();
        this.description.put(lang, description);
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isInternetStreaming() {
        return internetStreaming;
    }

    public void setInternetStreaming(boolean internetStreaming) {
        this.internetStreaming = internetStreaming;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeString(title.get("es"));
        out.writeString(title.get("ca"));
        out.writeString(title.get("en"));
        out.writeString(description.get("es"));
        out.writeString(description.get("ca"));
        out.writeString(description.get("en"));
        out.writeString(path);
        out.writeBooleanArray(new boolean[]{internetStreaming});
    }

    public static final Creator<Video> CREATOR = new Creator<Video>() {
        @Override
        public Video createFromParcel(Parcel parcel) {
            return new Video(parcel);
        }

        @Override
        public Video[] newArray(int i) {
            return new Video[i];
        }
    };

    public void readFromParcel(Parcel in) {
        title = new HashMap<String, String>();
        title.put("es", in.readString());
        title.put("ca", in.readString());
        title.put("en", in.readString());
        description = new HashMap<String, String>();
        description.put("es", in.readString());
        description.put("ca", in.readString());
        description.put("en", in.readString());
        path = in.readString();
        boolean[] bAux = new boolean[1];
        in.readBooleanArray(bAux);
        internetStreaming = bAux[0];
    }
}
