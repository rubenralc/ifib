package com.rubenralc.ifib.app;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.rubenralc.ifib.app.adapters.ShoppingCartItemAdapter;
import com.rubenralc.ifib.app.database.IMuseumsSQLiteHelper;
import com.rubenralc.ifib.app.fragments.DeletingShoppingCartItemFragment;
import com.rubenralc.ifib.app.model.ShoppingCartItem;

import java.util.ArrayList;

/**
 * Created by Ruben on 27/11/13.
 */
public class ShoppingCart extends MuseumsActivity implements DeletingShoppingCartItemFragment.EventListener {

    private ListView listShoppingCart;

    private int selected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shopping_cart);

        getActionBar().setDisplayHomeAsUpEnabled(true);

        listShoppingCart = (ListView) findViewById(R.id.shopping_cart_list);

        museum = getIntent().getStringExtra("museum");

        IMuseumsSQLiteHelper sqLiteHelper = IMuseumsSQLiteHelper.getInstance(this);

        SQLiteDatabase db = sqLiteHelper.getWritableDatabase();

        if (db != null) {

            Cursor c = db.rawQuery("SELECT * FROM ShoppingCartItems WHERE museum='" + museum + "'", null);

            ArrayList<ShoppingCartItem> shoppingCartItems = new ArrayList<ShoppingCartItem>();

            if (c.moveToFirst()) {
                do {
                    ShoppingCartItem shoppingCartItem = new ShoppingCartItem();
                    shoppingCartItem.setId(c.getString(0));
                    shoppingCartItem.setName(c.getString(1));
                    shoppingCartItem.setImg(c.getString(2));
                    shoppingCartItem.setMuseum(museum);
                    shoppingCartItem.setQuantity(Integer.parseInt(c.getString(4)));

                    shoppingCartItems.add(shoppingCartItem);
                } while (c.moveToNext());
            }

            listShoppingCart.setAdapter(new ShoppingCartItemAdapter(getBaseContext(), shoppingCartItems));
            listShoppingCart.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    selected = position;
                    DialogFragment dialogFragment = DeletingShoppingCartItemFragment.newInstance(((ShoppingCartItem) listShoppingCart.getItemAtPosition(selected)).getName());
                    dialogFragment.show(getSupportFragmentManager(), "deleteItem");
                }
            });
        }
    }

    @Override
    public boolean onNavigateUp() {
        finish();
        return true;
    }

    @Override
    public void deleteItem(DialogFragment dialog) {
        IMuseumsSQLiteHelper sqLiteHelper = IMuseumsSQLiteHelper.getInstance(this);

        SQLiteDatabase db = sqLiteHelper.getWritableDatabase();
        if (db != null) {
            db.execSQL("DELETE FROM ShoppingCartItems WHERE museum='" + museum + "' AND id='" + ((ShoppingCartItem) listShoppingCart.getItemAtPosition(selected)).getId() + "'");
        }

        finish();
        startActivityForResult(getIntent(), 0);
    }
}
