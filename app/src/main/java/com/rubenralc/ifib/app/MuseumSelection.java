package com.rubenralc.ifib.app;

import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.rubenralc.ifib.app.adapters.MuseumAdapter;
import com.rubenralc.ifib.app.adapters.RouteAdapter;
import com.rubenralc.ifib.app.model.Museum;
import com.rubenralc.ifib.app.model.Route;
import com.rubenralc.ifib.app.model.Sponsor;
import com.rubenralc.ifib.app.model.Video;
import com.rubenralc.ifib.app.utils.Images;
import com.rubenralc.ifib.app.utils.JsonHelper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class MuseumSelection extends MuseumsActivity {

    private Spinner museumSelector;
    private Spinner routeSelector;
    private HorizontalScrollView sponsorCarousel;
    private LinearLayout sponsorCarouselOuter;
    private ImageButton enter;
    private ImageButton favorites;
    private ImageButton videos;
    private ImageView museumImg;
    private TextView museumDescription;
    private TextView sponsorLiteral;

    private JsonHelper jsonHelper;

    private Thread scroller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.museum_selection);

        museumSelector = (Spinner) findViewById(R.id.museum_selector);
        routeSelector = (Spinner) findViewById(R.id.route_selector);
        sponsorCarousel = (HorizontalScrollView) findViewById(R.id.sponsor_carousel);
        sponsorCarouselOuter = (LinearLayout) findViewById(R.id.sponsor_carousel_outer);
        enter = (ImageButton) findViewById(R.id.enter);
        favorites = (ImageButton) findViewById(R.id.favorites);
        videos = (ImageButton) findViewById(R.id.museum_videos);
        museumImg = (ImageView) findViewById(R.id.museum_img);
        museumDescription = (TextView) findViewById(R.id.museum_description);
        sponsorLiteral = (TextView) findViewById(R.id.sponsor_literal);

        try {
            jsonHelper = new JsonHelper(getAssets());

            ArrayList<Museum> museums = jsonHelper.getMuseums();

            museumSelector.setAdapter(new MuseumAdapter(getBaseContext(), museums));
            museumSelector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                    museum = ((Museum) adapterView.getSelectedItem()).getName();
                    shoppingCart = ((Museum) adapterView.getSelectedItem()).isShoppingCart();

                    String language = getResources().getConfiguration().locale.getLanguage();

                    sponsorLiteral.setText(((Museum) adapterView.getSelectedItem()).getSponsorsLiteral(language));


                    try {
                        if (scroller != null)
                            scroller.interrupt();

                        addSponsors();

                        ArrayList<Video> museumVideos = jsonHelper.getMuseumVideos(museum);
                        if (museumVideos == null || museumVideos.size() <= 0)
                            videos.setVisibility(View.GONE);
                        else
                            videos.setVisibility(View.VISIBLE);

                        ViewTreeObserver vto = sponsorCarouselOuter.getViewTreeObserver();
                        if (vto != null) {
                            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                                @Override
                                public void onGlobalLayout() {
                                    sponsorCarouselOuter.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                                    startCarousel();
                                }
                            });
                        }

                        ArrayList<Route> routes = jsonHelper.getRoutes(((Museum) adapterView.getSelectedItem()).getName());
                        setRoutes(routes);
                        if (museumImg != null) {
                            museumImg.setImageDrawable(Images.getImageDrawable(getAssets(), getResources(), museum + "/" + ((Museum) adapterView.getSelectedItem()).getImg()));
                        }
                        if (museumDescription != null) {
                            museumDescription.setText(((Museum) adapterView.getSelectedItem()).getDescription(language));
                        }
                    } catch (Exception ex) {
                        onNothingSelected(adapterView);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                    ArrayList<Route> routes = new ArrayList<Route>();
                    setRoutes(routes);
                    sponsorLiteral.setText("");
                    addSponsors();
                }

                private void setRoutes(ArrayList<Route> routes) {
                    routeSelector.setAdapter(new RouteAdapter(getBaseContext(), routes));
                }
            });

        } catch (Exception exception) {
            exception.printStackTrace();
        }

        sponsorCarousel.setHorizontalScrollBarEnabled(false);
        sponsorCarousel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        addSponsors();

        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                museum = ((Museum) museumSelector.getSelectedItem()).getName();
                shoppingCart = ((Museum) museumSelector.getSelectedItem()).isShoppingCart();
                Route route = (Route) routeSelector.getSelectedItem();

                Intent goToRoomSelection = new Intent(view.getContext(), MapRoomSelection.class);
                Bundle passParams = new Bundle();
                passParams.putString("museum", museum);
                passParams.putBoolean("shoppingcart", shoppingCart);
                passParams.putParcelable("route", route);
                MuseumsApplication museumsApplication = (MuseumsApplication) getApplication();
                museumsApplication.setSelectedRoute(route);
                goToRoomSelection.putExtras(passParams);

                startActivityForResult(goToRoomSelection, 0);
            }
        });

        favorites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                museum = ((Museum) museumSelector.getSelectedItem()).getName();
                shoppingCart = ((Museum) museumSelector.getSelectedItem()).isShoppingCart();
                Route route = (Route) routeSelector.getSelectedItem();

                Intent goToFavorites = new Intent(v.getContext(), Favorites.class);
                Bundle passParams = new Bundle();
                passParams.putString("museum", museum);
                passParams.putBoolean("shoppingcart", shoppingCart);
                passParams.putParcelable("route", route);
                goToFavorites.putExtras(passParams);

                startActivityForResult(goToFavorites, 0);
            }
        });

        videos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                museum = ((Museum) museumSelector.getSelectedItem()).getName();
                shoppingCart = ((Museum) museumSelector.getSelectedItem()).isShoppingCart();
                Route route = (Route) routeSelector.getSelectedItem();

                Intent goToArtworkVideos = new Intent(getBaseContext(), VideoList.class);
                Bundle extras = new Bundle();
                try {
                    extras.putParcelableArrayList("videos", jsonHelper.getMuseumVideos(museum));
                    extras.putString("museum", museum);
                    extras.putBoolean("shoppingcart", shoppingCart);
                    extras.putString("artwork", museum);
                    goToArtworkVideos.putExtras(extras);

                    startActivityForResult(goToArtworkVideos, 0);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        ViewTreeObserver vto = sponsorCarouselOuter.getViewTreeObserver();
        if (vto != null) {
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    sponsorCarouselOuter.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    startCarousel();
                }
            });
        }
    }

    private void addSponsors() {
        sponsorCarouselOuter.removeAllViews();
        try {
            ArrayList<Sponsor> sponsors = jsonHelper.getSponsors(((Museum) museumSelector.getSelectedItem()).getName());

            for (final Sponsor sponsor : sponsors) {
                final Button imageButton = new Button(this);

                Drawable drawable = getSponsorIcon(sponsor);

                imageButton.setBackground(drawable);
                imageButton.setTag(sponsor.getName());
                imageButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(sponsor.getUrl())));
                    }
                });

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(150, 150);
                params.setMargins(25, 25, 25, 25);
                imageButton.setLayoutParams(params);
                sponsorCarouselOuter.addView(imageButton);
            }
        } catch (Exception exception) {
            sponsorCarouselOuter.removeAllViews();
        }
    }

    private Drawable getSponsorIcon(Sponsor sponsor) {
        Bitmap bm = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;
        AssetFileDescriptor fileDescriptor = null;
        try {
            fileDescriptor = this.getContentResolver().openAssetFileDescriptor(Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/imuseums/" + ((Museum) museumSelector.getSelectedItem()).getName() + "/" + sponsor.getImg())), "r");

        } catch (FileNotFoundException exception) {
            exception.printStackTrace();
        } finally {
            try {
                bm = BitmapFactory.decodeFileDescriptor(fileDescriptor.getFileDescriptor());
                fileDescriptor.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
        return new BitmapDrawable(getResources(), bm);
    }

    private void startCarousel() {
        sponsorCarousel.smoothScrollTo(0, 0);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        final int maxScroll = sponsorCarousel.getChildAt(0).getMeasuredWidth() - size.x;
        scroller = null;
        scroller = new Thread() {
            private boolean right = true;
            private boolean running = true;

            public void terminate() {
                running = false;
            }

            @Override
            public synchronized void start() {
                super.start();
                running = true;
            }

            @Override
            public void run() {
                try {
                    while (running) {
                        sleep(100);
                        if (right) {
                            if (sponsorCarousel.getScrollX() < maxScroll) {
                                sponsorCarousel.smoothScrollBy(2, 0);
                            } else {
                                right = false;
                            }
                        } else {
                            if (sponsorCarousel.getScrollX() > 0) {
                                sponsorCarousel.smoothScrollBy(-2, 0);
                            } else {
                                right = true;
                            }
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        if (scroller != null && maxScroll > 0)
            scroller.start();
    }

}
