package com.rubenralc.ifib.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.rubenralc.ifib.app.R;
import com.rubenralc.ifib.app.model.Museum;
import com.rubenralc.ifib.app.utils.Images;

import java.util.ArrayList;

/**
 * Created by Ruben on 8/07/13.
 */
public class MuseumAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<Museum> mMuseums;

    public MuseumAdapter(Context context, ArrayList<Museum> museums) {
        this.mContext = context;
        this.mMuseums = museums;
    }

    @Override
    public int getCount() {
        return mMuseums.size();
    }

    @Override
    public Object getItem(int i) {
        return mMuseums.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        MuseumViewHolder museumViewHolder;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.museum_item, null);

            museumViewHolder = new MuseumViewHolder();

            museumViewHolder.name = (TextView) view.findViewById(R.id.museum_name);
            museumViewHolder.largeName = (TextView) view.findViewById(R.id.museum_large_name);
            museumViewHolder.logo = (ImageView) view.findViewById(R.id.museum_img);

            view.setTag(museumViewHolder);
        } else {
            museumViewHolder = (MuseumViewHolder) view.getTag();
        }

        Museum museum = mMuseums.get(i);
        if (museum != null) {
            museumViewHolder.name.setText(museum.getName());
            museumViewHolder.largeName.setText(museum.getLargeName());
            museumViewHolder.logo.setImageDrawable(Images.getImageDrawable(mContext.getAssets(), mContext.getResources(), museum.getName() + "/" + museum.getImg()));
        }

        return view;
    }

    static class MuseumViewHolder {
        TextView name;
        TextView largeName;
        ImageView logo;
    }
}
