package com.rubenralc.ifib.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.rubenralc.ifib.app.R;
import com.rubenralc.ifib.app.model.Video;

import java.util.ArrayList;

/**
 * Created by Ruben on 2/07/13.
 */
public class VideoAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Video> mVideos;

    public VideoAdapter(Context context, ArrayList<Video> videos) {
        this.context = context;
        this.mVideos = videos;
    }

    @Override
    public int getCount() {
        return mVideos.size();
    }

    @Override
    public Object getItem(int i) {
        return mVideos.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        VideoViewHolder videoViewHolder;

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.video_item, null);

            videoViewHolder = new VideoViewHolder();
            videoViewHolder.title = (TextView) view.findViewById(R.id.video_title);
            videoViewHolder.description = (TextView) view.findViewById(R.id.video_description);

            view.setTag(videoViewHolder);
        } else {
            videoViewHolder = (VideoViewHolder) view.getTag();
        }

        Video video = mVideos.get(i);
        if (video != null) {
            videoViewHolder.title.setText(video.getTitle(context.getResources().getConfiguration().locale.getLanguage()));
            videoViewHolder.description.setText(video.getDescription(context.getResources().getConfiguration().locale.getLanguage()));
        }

        return view;
    }

    static class VideoViewHolder {
        TextView title;
        TextView description;
    }
}
