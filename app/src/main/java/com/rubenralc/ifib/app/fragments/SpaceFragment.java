package com.rubenralc.ifib.app.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;

import com.rubenralc.ifib.app.ArtworkDetail;
import com.rubenralc.ifib.app.MuseumsActivity;
import com.rubenralc.ifib.app.R;
import com.rubenralc.ifib.app.model.Area;
import com.rubenralc.ifib.app.model.Artwork;
import com.rubenralc.ifib.app.model.Door;
import com.rubenralc.ifib.app.model.Room;
import com.rubenralc.ifib.app.model.Route;
import com.rubenralc.ifib.app.model.Step;
import com.rubenralc.ifib.app.utils.JsonHelper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by ruben.rodriguez on 23/08/13.
 */
public class SpaceFragment extends Fragment {
    private Context context;
    private MuseumsActivity activity;

    private TableLayout space;

    private String museum;
    private ArrayList<Artwork> artworks;
    private ArrayList<Door> doors;
    private Boolean shoppingCart;
    private ArrayList<String> preferredArtworks;
    private Route route;
    private Paint paint;

    public SpaceFragment() {
    }

    public SpaceFragment(Context context, MuseumsActivity activity, String museum, ArrayList<Artwork> artworks, ArrayList<Door> doors, Boolean shoppingCart, ArrayList<String> preferredArtworks, Route route) {
        this.context = context;
        this.activity = activity;
        this.museum = museum;
        this.artworks = artworks;
        this.doors = doors;
        this.shoppingCart = shoppingCart;
        this.preferredArtworks = preferredArtworks;
        this.route = route;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.space_fragment, container, false);

        space = (TableLayout) v.findViewById(R.id.space);

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        int numArtworks = artworks.size();
        int numDoors = doors.size();

        Point screenSize = new Point();
        getActivity().getWindowManager().getDefaultDisplay().getSize(screenSize);

        int layoutHeight = screenSize.y;
        int layoutWidth = screenSize.x;

        TypedValue tv = new TypedValue();
        if (getActivity().getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            int actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
            layoutHeight -= actionBarHeight;
        }

        TableRow tableRow = new TableRow(context);
        tableRow.setPadding(0, 0, 0, 0);
        TableRow.LayoutParams layoutParams = new TableRow.LayoutParams((layoutWidth / (numArtworks + numDoors)) - 50, (layoutHeight) - 50);
        layoutParams.setMargins(25, 25, 25, 25);

        LinearLayout artworkLayout;
        for (int i = 0; i < numArtworks; i++) {
            artworkLayout = new LinearLayout(context);
            artworkLayout.setLayoutParams(layoutParams);
            artworkLayout.setOrientation(LinearLayout.VERTICAL);
            ImageView preferredOrder = new ImageView(context);
            ImageView image = new ImageView(context);

            final Artwork artwork = artworks.get(i);

            Bitmap bm = ((BitmapDrawable) getArtworkIcon(artwork.getId(), artwork.getImg())).getBitmap();
            Bitmap orderBm = BitmapFactory.decodeResource(getResources(), R.drawable.order_marker);

            Bitmap mBitmap = bm.copy(Bitmap.Config.ARGB_8888, true);

            if (preferredArtworks.contains(artwork.getId())) {
                orderBm = orderBm.copy(Bitmap.Config.ARGB_8888, true);
                Canvas canvas = new Canvas(orderBm);
                Paint paintText = new Paint(Paint.ANTI_ALIAS_FLAG);
                paintText.setColor(Color.BLACK);
                paintText.setTextSize(24.0f);

                Rect bounds = new Rect();
                paintText.getTextBounds(String.valueOf(preferredArtworks.indexOf(artwork.getId()) + 1), 0, String.valueOf(preferredArtworks.indexOf(artwork.getId()) + 1).length(), bounds);
                int x = (orderBm.getWidth() - bounds.width()) / 2;
                int y = (orderBm.getHeight() + bounds.height()) / 2;

                canvas.drawText(String.valueOf(preferredArtworks.indexOf(artwork.getId()) + 1), x, y, paintText);

                preferredOrder.setImageBitmap(orderBm);
                artworkLayout.addView(preferredOrder);
            }

            image.setImageDrawable(new BitmapDrawable(getResources(), mBitmap));
            //image.setLayoutParams(layoutParams);
            image.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    return getActivity().onTouchEvent(motionEvent);
                }
            });
            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent goToArtwork = new Intent(context, ArtworkDetail.class);
                    Bundle extras = new Bundle();
                    extras.putParcelable("artwork", artwork);
                    extras.putString("museum", museum);
                    extras.putBoolean("shoppingcart", shoppingCart);
                    goToArtwork.putExtras(extras);
                    startActivityForResult(goToArtwork, 0);
                }
            });
            artworkLayout.addView(image);
            tableRow.addView(artworkLayout);
        }

        for (int i = 0; i < numDoors; i++) {
            artworkLayout = new LinearLayout(context);
            artworkLayout.setLayoutParams(layoutParams);
            ImageView image = new ImageView(context);

            final Door door = doors.get(i);
            image.setImageDrawable(getArtworkIcon(door.getId(), door.getImg()));
            //image.setLayoutParams(layoutParams);
            image.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    return getActivity().onTouchEvent(motionEvent);
                }
            });
            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try {
                        JsonHelper jsonHelper = new JsonHelper(context.getAssets());
                        ArrayList<Area> areas = jsonHelper.getAreas(museum);
                        Room room = null;
                        for (Area area : areas) {
                            if (room == null)
                                room = area.getRoom(door.getDestinyRoom());
                        }
                        if (room != null) {
                            Intent goToArtwork = new Intent(context, ArtworkDetail.class);
                            Bundle extras = new Bundle();
                            extras.putParcelable("room", room);
                            extras.putString("museum", museum);
                            extras.putBoolean("shoppingcart", shoppingCart);for(Step s: route.getSteps()){
                                if(s.getRoom() == room.getId())
                                    preferredArtworks = s.getPreferredArtworks();
                            }
                            extras.putStringArrayList("preferredartworks", preferredArtworks);
                            goToArtwork.putExtras(extras);
                            startActivityForResult(goToArtwork, 0);
                            activity.finish();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });

            artworkLayout.addView(image);
            tableRow.addView(artworkLayout);
        }

        space.addView(tableRow, new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
    }

    private Drawable getArtworkIcon(String name, String img) {
        Bitmap bm = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;
        AssetFileDescriptor fileDescriptor = null;
        try {
            fileDescriptor = context.getContentResolver().openAssetFileDescriptor(Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/imuseums/" + museum + "/" + name + "/" + img)), "r");

        } catch (FileNotFoundException exception) {
            exception.printStackTrace();
        } finally {
            try {
                bm = BitmapFactory.decodeFileDescriptor(fileDescriptor.getFileDescriptor());
                fileDescriptor.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
        return new BitmapDrawable(getResources(), bm);
    }
}
