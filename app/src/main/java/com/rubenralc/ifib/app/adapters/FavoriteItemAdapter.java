package com.rubenralc.ifib.app.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.rubenralc.ifib.app.R;
import com.rubenralc.ifib.app.model.FavoriteItem;
import com.rubenralc.ifib.app.utils.Images;

import java.util.ArrayList;

/**
 * Created by Ruben on 4/01/14.
 */
public class FavoriteItemAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<FavoriteItem> favoriteItems;

    public FavoriteItemAdapter(Context context, ArrayList<FavoriteItem> favoriteItems) {
        this.context = context;
        this.favoriteItems = favoriteItems;
    }

    @Override
    public int getCount() {
        return favoriteItems.size();
    }

    @Override
    public Object getItem(int position) {
        return favoriteItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        FavoriteItemViewHolder favoriteItemViewHolder;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.favorites_item, null);

            favoriteItemViewHolder = new FavoriteItemViewHolder();

            favoriteItemViewHolder.itemImg = (ImageView) view.findViewById(R.id.item_img);
            favoriteItemViewHolder.itemName = (TextView) view.findViewById(R.id.item_name);
            favoriteItemViewHolder.itemPunctuation = (ImageView) view.findViewById(R.id.item_punctuation);

            view.setTag(favoriteItemViewHolder);
        } else {
            favoriteItemViewHolder = (FavoriteItemViewHolder) view.getTag();
        }

        FavoriteItem favoriteItem = favoriteItems.get(position);
        if (favoriteItem != null) {
            //favoriteItemViewHolder.itemImg.setImageDrawable(Images.getImageDrawable(context.getResources(), context.getContentResolver(), Environment.getExternalStorageDirectory() + "/imuseums/" + favoriteItem.getMuseum() + "/" + favoriteItem.getId() + "/" + favoriteItem.getImg()));
            favoriteItemViewHolder.itemName.setText(favoriteItem.getName());

            int points = favoriteItem.getPunctuation();

            Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.star_fav_full);

            bitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
            Canvas canvas = new Canvas(bitmap);
            Paint paintText = new Paint(Paint.ANTI_ALIAS_FLAG);
            paintText.setColor(Color.BLACK);
            paintText.setTextSize(30.0f);

            Rect bounds = new Rect();
            paintText.getTextBounds(String.valueOf(points), 0, String.valueOf(points).length(), bounds);
            int x = (bitmap.getWidth() - bounds.width()) / 2;
            int y = (bitmap.getHeight() + bounds.height()) / 2;

            canvas.drawText(String.valueOf(points), x, y, paintText);

            favoriteItemViewHolder.itemPunctuation.setImageBitmap(bitmap);
        }

        return view;
    }

    static class FavoriteItemViewHolder {
        ImageView itemImg;
        ImageView itemPunctuation;
        TextView itemName;
    }
}