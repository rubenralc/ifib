package com.rubenralc.ifib.app;

import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.rubenralc.ifib.app.database.IMuseumsSQLiteHelper;
import com.rubenralc.ifib.app.fragments.FavoritesStarsFragment;
import com.rubenralc.ifib.app.model.Artwork;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by ruben.rodriguez on 25/06/13.
 */
public class ArtworkDetail extends MuseumsActivity {

    private LinearLayout descriptionBtn;
    private LinearLayout videosBtn;
    private LinearLayout favoritesBtn;
    private ImageView artworkImg;
    private ImageView favStar;

    private Artwork artwork;

    private FavoritesStarsFragment favoritesStarsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.artwork_detail);

        getActionBar().setDisplayHomeAsUpEnabled(true);

        final String language = getResources().getConfiguration().locale.getLanguage();

        artwork = getIntent().getParcelableExtra("artwork");
        museum = getIntent().getStringExtra("museum");
        shoppingCart = getIntent().getBooleanExtra("shoppingcart", false);

        getActionBar().setTitle(artwork.getName(language));

        descriptionBtn = (LinearLayout) findViewById(R.id.description_btn);
        videosBtn = (LinearLayout) findViewById(R.id.videos_btn);
        favoritesBtn = (LinearLayout) findViewById(R.id.fav_btn);
        artworkImg = (ImageView) findViewById(R.id.artwork_img);
        favStar = (ImageView) findViewById(R.id.fav_star);

        BitmapDrawable artworkIcon = (BitmapDrawable) getArtworkIcon(artwork.getId(), artwork.getImg());

        if (artworkIcon.getBitmap().getHeight() >= artworkIcon.getBitmap().getWidth()) {
            getResources().getConfiguration().orientation = Configuration.ORIENTATION_PORTRAIT;
        } else {
            getResources().getConfiguration().orientation = Configuration.ORIENTATION_LANDSCAPE;
        }

        artworkImg.setImageDrawable(getArtworkIcon(artwork.getId(), artwork.getImg()));

        descriptionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent goToArtworkDescription = new Intent(getBaseContext(), ArtworkDescription.class);
                Bundle extras = new Bundle();
                extras.putString("description", artwork.getTxt(language));
                extras.putString("museum", museum);
                extras.putBoolean("shoppingcart", shoppingCart);
                extras.putString("artwork", artwork.getId());
                goToArtworkDescription.putExtras(extras);

                startActivityForResult(goToArtworkDescription, 0);
            }
        });

        if (artwork.getVideos().size() <= 0)
            videosBtn.setVisibility(View.INVISIBLE);
        else
            videosBtn.setVisibility(View.VISIBLE);

        videosBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent goToArtworkVideos = new Intent(getBaseContext(), VideoList.class);
                Bundle extras = new Bundle();
                extras.putParcelableArrayList("videos", artwork.getVideos());
                extras.putString("museum", museum);
                extras.putBoolean("shoppingcart", shoppingCart);
                extras.putString("artwork", artwork.getId());
                goToArtworkVideos.putExtras(extras);

                startActivityForResult(goToArtworkVideos, 0);
            }
        });

        favoritesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                favoritesStarsFragment = new FavoritesStarsFragment(getBaseContext(), museum, artwork);
                getSupportFragmentManager().beginTransaction().add(R.id.fav_fragment_placer, favoritesStarsFragment).commit();
            }
        });

        IMuseumsSQLiteHelper sqLiteHelper = IMuseumsSQLiteHelper.getInstance(this);

        SQLiteDatabase db = sqLiteHelper.getWritableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM Favorites WHERE id='" + artwork.getId() + "' AND museum='" + museum + "'", null);

        if (c.moveToFirst()) {
            int points = c.getInt(c.getColumnIndex("points"));

            if (points > 0) {
                Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.star_fav_full);

                bitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
                Canvas canvas = new Canvas(bitmap);
                Paint paintText = new Paint(Paint.ANTI_ALIAS_FLAG);
                paintText.setColor(Color.BLACK);
                paintText.setTextSize(30.0f);

                Rect bounds = new Rect();
                paintText.getTextBounds(String.valueOf(points), 0, String.valueOf(points).length(), bounds);
                int x = (bitmap.getWidth() - bounds.width()) / 2;
                int y = (bitmap.getHeight() + bounds.height()) / 2;

                canvas.drawText(String.valueOf(points), x, y, paintText);

                favStar.setImageBitmap(bitmap);
            }
        }
    }

    @Override
    public void onCloseFragment(String tag) {
        if (favoritesStarsFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(favoritesStarsFragment).commit();
            favoritesStarsFragment = null;
            finish();
            startActivityForResult(getIntent(), 0);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.artwork_detail, menu);
        MenuItem goShopping = menu.findItem(R.id.go_shopping);
        if (goShopping != null) goShopping.setVisible(shoppingCart);
        MenuItem addShoppingCart = menu.findItem(R.id.add_shopping);
        if (addShoppingCart != null) addShoppingCart.setVisible(shoppingCart);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.add_shopping:
                addArtworkShopping();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void addArtworkShopping() {

        IMuseumsSQLiteHelper sqLiteHelper = IMuseumsSQLiteHelper.getInstance(this);

        SQLiteDatabase db = sqLiteHelper.getWritableDatabase();

        if (db != null) {

            SQLiteStatement sqLiteStatement = db.compileStatement("select quantity from ShoppingCartItems where id = '" + artwork.getId() + "' and museum = '" + museum + "'");

            long quant = 0;
            try {
                quant = sqLiteStatement.simpleQueryForLong();
            } catch (Exception sqlEx) {

            }
            if (quant <= 0) {
                String sqlInsert = "INSERT INTO ShoppingCartItems (id, name, img, museum, quantity) VALUES ('" + artwork.getId() + "', '" + artwork.getName("es") + "', '" + artwork.getImg() + "', '" + museum + "', 1)";
                db.execSQL(sqlInsert);
            } else {
                quant += 1;
                String sqlUpdate = "UPDATE ShoppingCartItems SET quantity = " + quant + " WHERE id = '" + artwork.getId() + "' and museum = '" + museum + "'";
                db.execSQL(sqlUpdate);
            }
            Toast.makeText(this, R.string.artwork_shopped, Toast.LENGTH_LONG).show();
        }

        db.close();
    }

    @Override
    public boolean onNavigateUp() {
        finish();
        return true;
    }

    private Drawable getArtworkIcon(String name, String img) {
        Bitmap bm = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;
        AssetFileDescriptor fileDescriptor = null;
        try {
            fileDescriptor = this.getContentResolver().openAssetFileDescriptor(Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/imuseums/" + museum + "/" + name + "/" + img)), "r");

        } catch (FileNotFoundException exception) {
            exception.printStackTrace();
        } finally {
            try {
                bm = BitmapFactory.decodeFileDescriptor(fileDescriptor.getFileDescriptor());
                fileDescriptor.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
        return new BitmapDrawable(getResources(), bm);
    }
}
