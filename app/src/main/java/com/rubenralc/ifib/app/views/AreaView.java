package com.rubenralc.ifib.app.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.rubenralc.ifib.app.model.Area;
import com.rubenralc.ifib.app.model.Door;
import com.rubenralc.ifib.app.model.Room;
import com.rubenralc.ifib.app.model.Step;
import com.rubenralc.ifib.app.threads.AreaThread;

import java.util.ArrayList;

/**
 * Created by ruben.rodriguez on 26/08/13.
 */
public class AreaView extends SurfaceView implements SurfaceHolder.Callback {

    private AreaThread thread;

    private Area area;
    private ArrayList<Step> steps;
    private String museum;

    private float startX = 0f;
    private float startY = 0f;

    private float translateX = 0f;
    private float translateY = 0f;

    private float previousTranslateX = 0f;
    private float previousTranslateY = 0f;

    private float ratioWidth = 0f;
    private float ratioHeight = 0f;

    public AreaView(Context context) {
        super(context);
        getHolder().addCallback(this);
    }

    public AreaView(Context context, AttributeSet attrs) {
        super(context, attrs);
        getHolder().addCallback(this);
    }

    public AreaView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        getHolder().addCallback(this);
    }

    public void setMuseum(String museum) {
        this.museum = museum;
    }

    public String getMuseum() {
        return museum;
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        thread = new AreaThread(getHolder(), this);
        thread.setRunning(true);
        thread.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        boolean retry = true;
        thread.setRunning(false);
        while (retry) {
            try {
                thread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
    }

    public void setArea(Area area) {
        this.area = area;
        this.area.setState(getContext(), museum);
    }

    public Area getArea() {
        return this.area;
    }

    public void setSteps(ArrayList<Step> steps) {
        this.steps = steps;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startX = event.getX() - previousTranslateX;
                startY = event.getY() - previousTranslateY;
                break;
            case MotionEvent.ACTION_MOVE:
                previousTranslateX = translateX;
                previousTranslateY = translateY;
                translateX = event.getX() - startX;
                translateY = event.getY() - startY;
                break;
            case MotionEvent.ACTION_UP:
                previousTranslateX = translateX;
                previousTranslateY = translateY;
                break;
        }
        return true;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.save();

        if (canvas != null) {
            int ancho = canvas.getWidth() - 32;
            int alto = canvas.getHeight() - 32;

            canvas.drawColor(Color.WHITE);

            if (area != null) {
                int maxWidth = area.getMaxWidth();
                int maxHeight = area.getMaxHeight();


                int originalMaxWidth = maxWidth;
                int originalMaxHeight = maxHeight;

                if (maxWidth > 9)
                    maxWidth = 9;

                if (maxHeight > 15)
                    maxHeight = 15;

                ratioWidth = ancho / maxWidth;
                ratioHeight = alto / maxHeight;

                if (originalMaxWidth > 9 || originalMaxHeight > 15) {
                    float onCanvasWidth = (area.getMaxWidth() * ratioWidth) + 32;
                    float onCanvasHeight = (area.getMaxHeight() * ratioHeight) + 32;

                    Rect boundsBefore = canvas.getClipBounds();

                    canvas.translate(translateX, translateY);

                    Rect boundsAfter = canvas.getClipBounds();
                    if (originalMaxHeight > 15) {
                        if (boundsAfter.top < 0) {
                            canvas.translate(0, boundsAfter.top);
                            translateY = boundsBefore.top;
                        }

                        if (boundsAfter.bottom > onCanvasHeight) {
                            canvas.translate(0, -translateY);
                            translateY = boundsBefore.height() - onCanvasHeight;
                            canvas.translate(0, translateY);
                        }
                    }

                    if (originalMaxWidth > 9) {
                        if (boundsAfter.left < 0) {
                            canvas.translate(boundsAfter.left, 0);
                            translateX = boundsBefore.left;
                        }

                        if (boundsAfter.right > onCanvasWidth) {
                            canvas.translate(-translateX, 0);
                            translateX = boundsBefore.width() - onCanvasWidth;
                            canvas.translate(translateX, 0);
                        }
                    }
                }

                canvas = area.draw(getContext(), canvas, ratioWidth, ratioHeight, museum);

                if (steps != null) {
                    Step prevStep = steps.get(0);
                    Step actStep;

                    Room prevRoom = area.getRoom(prevStep.getArea(), prevStep.getRoom());
                    Room actRoom = null;

                    Paint line = new Paint();
                    line.setStrokeWidth(5);
                    line.setColor(Color.BLUE);

                    if (prevRoom != null) {
                        Door initialDoor = prevRoom.getDoorByDestiny("outside");
                        if (initialDoor != null)
                            canvas.drawLine(initialDoor.getCenterX(), initialDoor.getCenterY(), prevRoom.getCenterX(), prevRoom.getCenterY(), line);
                    }

                    for (int i = 1; i < steps.size(); i++) {
                        actStep = steps.get(i);
                        actRoom = area.getRoom(actStep.getArea(), actStep.getRoom());

                        if (prevRoom != null && actRoom != null) {
                            Door door = prevRoom.getDoorByDestiny(actRoom.getId());
                            if (door != null) {
                                canvas.drawLine(prevRoom.getCenterX(), prevRoom.getCenterY(), door.getCenterX(), door.getCenterY(), line);
                                canvas.drawLine(door.getCenterX(), door.getCenterY(), actRoom.getCenterX(), actRoom.getCenterY(), line);
                            }
                        }

                        prevRoom = actRoom;
                    }

                    if (actRoom != null) {
                        Door finalRoom = actRoom.getDoorByDestiny("outside");
                        if (finalRoom != null)
                            canvas.drawLine(actRoom.getCenterX(), actRoom.getCenterY(), finalRoom.getCenterX(), finalRoom.getCenterY(), line);
                    }
                }
            }

            getHolder().unlockCanvasAndPost(canvas);
        }
    }

    public ArrayList<Room> selectRooms(float pointedX, float pointedY) {
        ArrayList<Room> selectedRooms = new ArrayList<Room>();

        if (area != null)
            selectedRooms = area.select(pointedX, pointedY, translateX, translateY);

        return selectedRooms;
    }

    public void unSelectRooms() {
        thread.setRunning(false);

        if (area != null)
            area.unSelect();

        thread.setRunning(true);
    }
}
