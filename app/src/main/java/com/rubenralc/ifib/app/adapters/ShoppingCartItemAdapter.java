package com.rubenralc.ifib.app.adapters;

import android.content.Context;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.rubenralc.ifib.app.R;
import com.rubenralc.ifib.app.model.ShoppingCartItem;
import com.rubenralc.ifib.app.utils.Images;

import java.util.ArrayList;

/**
 * Created by Ruben on 27/11/13.
 */
public class ShoppingCartItemAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<ShoppingCartItem> shoppingCartItems;

    public ShoppingCartItemAdapter(Context context, ArrayList<ShoppingCartItem> shoppingCartItems) {
        this.context = context;
        this.shoppingCartItems = shoppingCartItems;
    }

    @Override
    public int getCount() {
        return shoppingCartItems.size();
    }

    @Override
    public Object getItem(int position) {
        return shoppingCartItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ShoppingCartItemViewHolder shoppingCartItemViewHolder;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.shopping_cart_item, null);

            shoppingCartItemViewHolder = new ShoppingCartItemViewHolder();

            shoppingCartItemViewHolder.itemImg = (ImageView) view.findViewById(R.id.item_img);
            shoppingCartItemViewHolder.itemName = (TextView) view.findViewById(R.id.item_name);
            shoppingCartItemViewHolder.itemId = (TextView) view.findViewById(R.id.item_id);

            view.setTag(shoppingCartItemViewHolder);
        } else {
            shoppingCartItemViewHolder = (ShoppingCartItemViewHolder) view.getTag();
        }

        ShoppingCartItem shoppingCartItem = shoppingCartItems.get(position);
        if (shoppingCartItem != null) {
            //shoppingCartItemViewHolder.itemImg.setImageDrawable(Images.getImageDrawable(context.getResources(), context.getContentResolver(), Environment.getExternalStorageDirectory() + "/imuseums/" + shoppingCartItem.getMuseum() + "/" + shoppingCartItem.getId() + "/" + shoppingCartItem.getImg()));
            shoppingCartItemViewHolder.itemName.setText(shoppingCartItem.getName());
            shoppingCartItemViewHolder.itemId.setText(shoppingCartItem.getId());
        }

        return view;
    }

    static class ShoppingCartItemViewHolder {
        ImageView itemImg;
        TextView itemId;
        TextView itemName;
    }
}
