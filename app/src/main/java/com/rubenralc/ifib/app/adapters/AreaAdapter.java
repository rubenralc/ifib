package com.rubenralc.ifib.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.rubenralc.ifib.app.R;
import com.rubenralc.ifib.app.model.Area;

import java.util.ArrayList;

/**
 * Created by Ruben on 19/06/13.
 */
public class AreaAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<Area> mAreas;

    public AreaAdapter(Context context, ArrayList<Area> areas) {
        mContext = context;
        mAreas = areas;
    }

    @Override
    public int getCount() {
        return mAreas.size();
    }

    @Override
    public Object getItem(int i) {
        return mAreas.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        AreaViewHolder areaViewHolder;

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.room_item, null);

            areaViewHolder = new AreaViewHolder();

            areaViewHolder.areaName = (TextView) view.findViewById(R.id.room_name);

            view.setTag(areaViewHolder);
        } else {
            areaViewHolder = (AreaViewHolder) view.getTag();
        }
        Area area = mAreas.get(i);
        if (area != null) {
            areaViewHolder.areaName.setText(area.getName());
        }

        return view;
    }

    private static class AreaViewHolder {
        TextView areaName;
    }
}
