package com.rubenralc.ifib.app.model.interfaces;

import android.content.Context;
import android.graphics.Canvas;

/**
 * Created by Ruben on 2/11/13.
 */
public interface IDrawable {
    Canvas draw(Context context, Canvas canvas);
}
