package com.rubenralc.ifib.app.model;

import android.os.Parcel;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ruben.rodriguez on 23/06/13.
 */
public class Artwork extends Element {
    private HashMap<String, String> name;
    private HashMap<String, String> txt;
    private int preferredOrder;
    private ArrayList<Video> videos;

    public Artwork() {
        super();
    }

    public Artwork(Parcel in) {
        super(in);
    }

    public void readFromParcel(Parcel in) {
        super.readFromParcel(in);
        videos = new ArrayList<Video>();
        name = new HashMap<String, String>();
        name.put("es", in.readString());
        name.put("ca", in.readString());
        name.put("en", in.readString());
        txt = new HashMap<String, String>();
        txt.put("es", in.readString());
        txt.put("ca", in.readString());
        txt.put("en", in.readString());
        preferredOrder = in.readInt();
        in.readTypedList(videos, Video.CREATOR);
    }

    public String getName(String lang) {
        return name.get(lang);
    }

    public void setName(String lang, String name) {
        if (this.name == null)
            this.name = new HashMap<String, String>();
        this.name.put(lang, name);
    }

    public String getTxt(String lang) {
        return txt.get(lang);
    }

    public void setTxt(String lang, String txt) {
        if (this.txt == null)
            this.txt = new HashMap<String, String>();
        this.txt.put(lang, txt);
    }

    public int getPreferredOrder() {
        return preferredOrder;
    }

    public void setPreferredOrder(int preferedOrder) {
        this.preferredOrder = preferedOrder;
    }

    public ArrayList<Video> getVideos() {
        return videos;
    }

    public void setVideos(ArrayList<Video> videos) {
        this.videos = videos;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeString(name.get("es"));
        out.writeString(name.get("ca"));
        out.writeString(name.get("en"));
        out.writeString(txt.get("es"));
        out.writeString(txt.get("ca"));
        out.writeString(txt.get("en"));
        out.writeInt(preferredOrder);
        out.writeTypedList(videos);
    }

    public static final Creator<Artwork> CREATOR = new Creator<Artwork>() {
        @Override
        public Artwork createFromParcel(Parcel parcel) {
            return new Artwork(parcel);
        }

        @Override
        public Artwork[] newArray(int i) {
            return new Artwork[i];
        }
    };
}
