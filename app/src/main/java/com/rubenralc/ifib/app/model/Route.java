package com.rubenralc.ifib.app.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Ruben on 15/06/13.
 */
public class Route implements Parcelable {
    private String id;
    private HashMap<String, String> name;
    private ArrayList<Step> steps;

    public Route() {

    }

    public Route(Parcel in) {
        steps = new ArrayList<Step>();
        readFromParcel(in);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName(String lang) {
        return name.get(lang);
    }

    public void setName(String lang, String name) {
        if (this.name == null)
            this.name = new HashMap<String, String>();
        this.name.put(lang, name);
    }

    public ArrayList<Step> getSteps() {
        return steps;
    }

    public void setSteps(ArrayList<Step> steps) {
        this.steps = steps;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(id);
        parcel.writeTypedList(steps);
    }

    public static final Creator<Route> CREATOR = new Creator<Route>() {
        @Override
        public Route createFromParcel(Parcel parcel) {
            return new Route(parcel);
        }

        @Override
        public Route[] newArray(int i) {
            return new Route[i];
        }
    };

    public void readFromParcel(Parcel in) {
        id = in.readString();
        in.readTypedList(steps, Step.CREATOR);
    }
}
