package com.rubenralc.ifib.app;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.rubenralc.ifib.app.adapters.VideoAdapter;
import com.rubenralc.ifib.app.model.Video;

import java.util.ArrayList;

/**
 * Created by Ruben on 1/07/13.
 */
public class VideoList extends MuseumsActivity {

    private ListView mListVideos;

    private ArrayList<Video> mVideos;
    private String artworkId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.artwork_videos);

        mListVideos = (ListView) findViewById(R.id.list_videos);

        mVideos = getIntent().getParcelableArrayListExtra("videos");
        museum = getIntent().getStringExtra("museum");
        shoppingCart = getIntent().getBooleanExtra("shoppingcart", false);
        artworkId = getIntent().getStringExtra("artwork");

        mListVideos.setAdapter(new VideoAdapter(getBaseContext(), mVideos));
        mListVideos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent goToVideoPlayer = new Intent(getBaseContext(), VideoPlayer.class);
                Bundle extras = new Bundle();
                extras.putParcelable("video", mVideos.get(i));
                extras.putString("museum", museum);
                extras.putString("artwork", artworkId);
                goToVideoPlayer.putExtras(extras);
                startActivityForResult(goToVideoPlayer, 0);
            }
        });
    }
}
