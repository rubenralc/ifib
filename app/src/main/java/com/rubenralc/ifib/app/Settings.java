package com.rubenralc.ifib.app;

import android.R;
import android.app.Activity;
import android.os.Bundle;

import com.rubenralc.ifib.app.fragments.SettingsFragment;

/**
 * Created by ruben.rodriguez on 21/08/13.
 */
public class Settings extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFragmentManager().beginTransaction().replace(R.id.content, new SettingsFragment()).commit();

    }
}
