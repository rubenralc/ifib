package com.rubenralc.ifib.app.utils;

import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Ruben on 6/03/14.
 */
public class AudioManager {

    private static AudioManager instance;

    public static AudioManager getInstance() {
        if(instance == null)
            instance = new AudioManager();
        return instance;
    }

    private AudioManager() {
    }

    public void saveMp3(byte[] byteSound, String filePath) throws IOException {
        FileOutputStream fos = new FileOutputStream(new File(Environment.getExternalStorageDirectory() + filePath));
        fos.write(byteSound);
        fos.close();
    }
}
