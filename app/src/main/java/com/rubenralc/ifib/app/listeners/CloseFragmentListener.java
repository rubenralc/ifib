package com.rubenralc.ifib.app.listeners;

/**
 * Created by ruben.rodriguez on 29/08/13.
 */
public interface CloseFragmentListener {
    void onCloseFragment(String tag);
}
